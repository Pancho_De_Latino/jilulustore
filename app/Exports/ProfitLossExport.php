<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProfitLossExport implements FromQuery, ShouldAutoSize
{
    use Exportable;

    public function __construct(String $fromdate, String $todate, Array $items, String $expenses)
    {
        $this->fromdate = $fromdate." 00:00:00";
        $this->todate = $todate." 23:59:59";
        $this->items = $items;
        $this->expenses = $expenses;
    }

    public function query()
    {
        $title = DB::table('items')->select(DB::raw('"ITEMNAME"'),DB::raw('"QUANTITY"'),DB::raw('"UNITPRICE"'),DB::raw('"SERIALNUMBER"'),DB::raw('"TOTALPRICE"'),DB::raw('"DATE"'),DB::raw('"CLIENT/SUPPLIER"'),DB::raw('"TRANSACTION"'))->orderBy("id")->limit(1);
        $a = DB::table('sale_items')
            ->leftJoin('sales','sale_items.sale_id','=','sales.id')
            ->leftJoin('item_stocks','item_stocks.id','=','sale_items.item_stock_id')
            ->leftJoin('items','items.id','=','item_stocks.item_id')
            ->select('items.item_name','sale_items.quantity','sale_items.amount','sales.receipt_number',DB::raw('sale_items.amount * sale_items.quantity as totalamount'),DB::raw('sales.created_at as transactiondate'),'sales.customer',DB::raw('"sale"'))
            ->whereIn('item_stocks.item_id',$this->items)
            ->where('sales.created_at','>=', $this->fromdate)
            ->where('sales.created_at','<=', $this->todate);

        $b = DB::table('grn_items')
            ->leftJoin('grns','grn_items.grn_id','=','grns.id')
            ->leftJoin('suppliers','suppliers.id','=','grns.supplier_id')
            ->leftJoin('items','items.id','=','grn_items.item_id')
            ->select('items.item_name','grn_items.quantity','grn_items.buying_price','grns.grn_number',DB::raw('grn_items.buying_price * grn_items.quantity as totalamount'),DB::raw('grns.created_at as transactiondate'),'suppliers.supplier_name',DB::raw('"purchase"'))
            ->whereIn('items.id',$this->items)
            ->where('grns.created_at','>=', $this->fromdate)
            ->where('grns.created_at','<=', $this->todate);
        if($this->expenses!=""){
            $expenditures = DB::table('expenses')
                ->leftJoin('expense_categories', 'expenses.expense_category','=','expense_categories.id')
                ->select('expense_categories.category_name','expenses.details',DB::raw('""'),DB::raw('""'),'expenses.amount','expenses.created_at',DB::raw('""'),DB::raw('"expenditures"'))
                ->where('expenses.created_at','>=', $this->fromdate)
                ->where('expenses.created_at','<=', $this->todate);
            $itemmovement = $a->union($b)->union($expenditures)->orderBy('transactiondate');

            $totalExpenditures = DB::table('expenses')
                ->leftJoin('expense_categories', 'expenses.expense_category','=','expense_categories.id')
                ->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"SUM OF EXPENSES"'),DB::raw('sum(expenses.amount) as sumofexpenses'),DB::raw('""'),DB::raw('""'),DB::raw('""'))
                ->where('expenses.created_at','>=', $this->fromdate)
                ->where('expenses.created_at','<=', $this->todate);
        } else {
            $itemmovement = $a->union($b)->orderBy('transactiondate');
        }
        $space = DB::table('items')->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'))->orderBy("id")->limit(1);

        // $itemmovement = $title->union($itemmovement)->union($space);

        $sumofa = DB::table('sale_items')
            ->leftJoin('sales','sale_items.sale_id','=','sales.id')
            ->leftJoin('item_stocks','item_stocks.id','=','sale_items.item_stock_id')
            ->leftJoin('items','items.id','=','item_stocks.item_id')
            ->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"SUM OF SALES"'),DB::raw('sum(sale_items.amount * sale_items.quantity) as sumofsales'),DB::raw('""'),DB::raw('""'),DB::raw('""'))
            ->whereIn('item_stocks.item_id',$this->items)
            ->where('sales.created_at','>=', $this->fromdate)
            ->where('sales.created_at','<=', $this->todate);

        $sumofb = DB::table('grn_items')
            ->leftJoin('grns','grn_items.grn_id','=','grns.id')
            ->leftJoin('suppliers','suppliers.id','=','grns.supplier_id')
            ->leftJoin('items','items.id','=','grn_items.item_id')
            ->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"SUM OF PURCHASES"'),DB::raw('sum(grn_items.buying_price * grn_items.quantity) as sumofpurchases'),DB::raw('""'),DB::raw('""'),DB::raw('""'))
            ->whereIn('items.id',$this->items)
            ->where('grns.created_at','>=', $this->fromdate)
            ->where('grns.created_at','<=', $this->todate);

        $sumofbuyingspersales = DB::table('sale_items')
            ->leftJoin('sales','sale_items.sale_id','=','sales.id')
            ->leftJoin('item_stocks','item_stocks.id','=','sale_items.item_stock_id')
            ->leftJoin('items','items.id','=','item_stocks.item_id')
            ->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"SUM OF BUYINGS PER SALES"'),DB::raw('sum(item_stocks.buying_price * sale_items.quantity) as sumofbuyings'),DB::raw('""'),DB::raw('""'),DB::raw('""'))
            ->whereIn('item_stocks.item_id',$this->items)
            ->where('sales.created_at','>=', $this->fromdate)
            ->where('sales.created_at','<=', $this->todate);
        $suma = $sumofa->get()[0];
        $sumb = $sumofb->get()[0];
        $sumbuyings = $sumofbuyingspersales->get()[0];

        $theprofit = (float)$suma->sumofsales - (float)$sumbuyings->sumofbuyings;



        if($this->expenses != ""){
            $sumofexpenses = $totalExpenditures->get()[0];
            $theprofit-=$sumofexpenses->sumofexpenses;
            $profit = DB::table('items')->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"PROFIT/LOSS"'),DB::raw('"'.$theprofit.'"'),DB::raw('""'),DB::raw('""'),DB::raw('""'))->orderBy("id")->limit(1);
            $itemmovement = $title->union($itemmovement)->union($space)->union($sumofa)->union($sumofbuyingspersales)->union($totalExpenditures)->union($profit);
            return $itemmovement;
        }
        $profit = DB::table('items')->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"PROFIT/LOSS"'),DB::raw('"'.$theprofit.'"'),DB::raw('""'),DB::raw('""'),DB::raw('""'))->orderBy("id")->limit(1);
        $itemmovement = $title->union($itemmovement)->union($space)->union($sumofa)->union($sumofbuyingspersales)->union($profit);
        return $itemmovement;
    }
}
