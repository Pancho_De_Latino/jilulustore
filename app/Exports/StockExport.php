<?php

namespace App\Exports;

use App\Models\Item;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StockExport implements FromQuery, ShouldAutoSize
{
    use Exportable;

    public function __construct(Array $items)
    {
        $this->items = $items;
    }

    public function query()
    {
        $title = DB::table('items')->select(DB::raw('"ITEMNAME"'),DB::raw('"SELLINGPRICE"'),DB::raw('"QUANTITY"'),DB::raw('"BUYINGPRICE"'),DB::raw('"TOTALBUYINGPRICE"'),DB::raw('"EXPIREDATE"'))->orderBy("id")->limit(1);

        $stock = DB::table('item_stocks')
            ->leftJoin('items','item_stocks.item_id','=','items.id')
            ->select('items.item_name','item_stocks.selling_price','item_stocks.quantity','item_stocks.buying_price',DB::raw('item_stocks.buying_price * item_stocks.quantity as totalbuyingprice'),'item_stocks.expire_date')
            ->whereIn('item_stocks.item_id',$this->items)
            ->where('item_stocks.quantity','>', 0)
            ->orderBy('items.item_name');

        $space = DB::table('items')->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('""'))->orderBy("id")->limit(1);

        $stockvalue = DB::table('item_stocks')
            ->leftJoin('items','item_stocks.item_id','=','items.id')
            ->select(DB::raw('""'),DB::raw('""'),DB::raw('""'),DB::raw('"STOCK VALUE"'),DB::raw('sum(item_stocks.buying_price * item_stocks.quantity) as totalbuyingprice'),DB::raw('""'))
            ->whereIn('item_stocks.item_id',$this->items)
            ->where('item_stocks.quantity','>', 0);

        $currentstock = $title->union($stock)->union($space)->union($stockvalue);
        return $currentstock;
    }
}
