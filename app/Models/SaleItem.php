<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class SaleItem extends Model
{
    use Uuids;
    public function sale_details(){
        return $this->belongsTo(Sale::class, 'sale_id');
    }

    public function item_stock_details(){
        return $this->belongsTo(ItemStock::class, 'item_stock_id');
    }
}
