<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class GrnStatus extends Model
{
    use Uuids;
}
