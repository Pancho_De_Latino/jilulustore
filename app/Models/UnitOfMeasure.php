<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class UnitOfMeasure extends Model
{
    use Uuids;
}
