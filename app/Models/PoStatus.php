<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class PoStatus extends Model
{
    use Uuids;
}
