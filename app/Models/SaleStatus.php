<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class SaleStatus extends Model
{
    use Uuids;
}
