<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class ItemStock extends Model
{
    use Uuids;
    public function item_details(){
        return $this->belongsTo(Item::class, 'item_id');
    }
}
