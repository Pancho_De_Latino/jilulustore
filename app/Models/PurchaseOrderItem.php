<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class PurchaseOrderItem extends Model
{
    use Uuids;
}
