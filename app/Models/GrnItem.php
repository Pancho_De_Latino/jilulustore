<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class GrnItem extends Model
{
    use Uuids;
}
