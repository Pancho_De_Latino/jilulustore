<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\User;

class Expense extends Model
{
    use Uuids;

    function expense_category_details(){
        return $this->belongsTo(ExpenseCategory::class, 'expense_category');
    }
    public function expense_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
