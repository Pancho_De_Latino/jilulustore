<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class ProfomaItem extends Model
{
    use Uuids;
}
