<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\User;

class Item extends Model
{
    use Uuids;

    public function item_uom(){
        return $this->belongsTo(UnitOfMeasure::class, 'unit_of_measure');
    }

    public function item_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function item_grns(){
        return $this->hasMany(GrnItem::class, 'item_id','id');
    }

    public function item_current_stock(){
        return $this->hasMany(ItemStock::class, "item_id","id");
    }
}
