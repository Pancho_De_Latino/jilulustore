<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\User;

class Grn extends Model
{
    use Uuids;

    public function grn_supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function grn_current_status(){
        return $this->belongsTo(GrnStatus::class, 'grn_status');
    }

    public function grn_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
