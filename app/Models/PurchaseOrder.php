<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\User;

class PurchaseOrder extends Model
{
    use Uuids;

    public function po_supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function po_current_status(){
        return $this->belongsTo(PoStatus::class, 'po_status');
    }

    public function po_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
