<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ExpenseCategory;
use App\Models\Supplier;
use App\Models\UnitOfMeasure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(){
        $unitofmeasures = UnitOfMeasure::all();

        $suppliers = Supplier::all();

        $expensecategories = ExpenseCategory::all();

        return view('settings', [
            'unitofmeasures' => $unitofmeasures,
            'suppliers' => $suppliers,
            'expensecategories' => $expensecategories
        ]);
    }
    function addUOM(Request $request){
        $this->validate($request,[
            'uom_name'=>['required'],
            'uom_short_name'=>['required'],
            'uom_quantity'=>['required']
        ]);
        // return $request;
        try{
            $unitofmeasure = new UnitOfMeasure();
            $unitofmeasure->uom_name = $request->uom_name;
            $unitofmeasure->uom_short_name = $request->uom_short_name;
            $unitofmeasure->uom_quantity = $request->uom_quantity;
            $unitofmeasure->created_by = Auth::user()->id;
            $unitofmeasure->save();
            return redirect()->back()->with('success', 'UOM is Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'UOM was not Saved');
        }
    }
    function deleteUOM(Request $request){
        $uom_id = $request->unitofmeasure;
        try{
            UnitOfMeasure::where('id', $uom_id)->delete();
            return redirect()->back()->with('success', 'UOM was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'UOM was not Deleted');
        }
    }
    function addSupplier(Request $request){
        $this->validate($request,[
            'supplier_name'=>['required'],
            'phone'=>['required'],
            'address'=>['required']
        ]);
        // return $request;
        try{
            $supplier = new Supplier();
            $supplier->supplier_name = $request->supplier_name;
            $supplier->phone = $request->phone;
            $supplier->address = $request->address;
            $supplier->created_by = Auth::user()->id;
            $supplier->save();
            return redirect()->back()->with('success', 'Supplier is Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'Supplier was not Saved');
        }
    }
    function deleteSupplier(Request $request){
        $supplier_id = $request->supplier_id;
        // return $supplier_id;
        try{
            Supplier::where('id', $supplier_id)->delete();
            return redirect()->back()->with('success', 'Supplier was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Supplier was not Deleted');
        }
    }
    function addExpenseCategory(Request $request){
        // return $request;
        $this->validate($request,[
            'category_name'=>['required'],
        ]);
        try{
            $expense_category = new ExpenseCategory();
            $expense_category->category_name = $request->category_name;
            $expense_category->created_by = Auth::user()->id;
            $expense_category->save();
            return redirect()->back()->with('success', 'Expense Category is Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'Expense Category was not Saved');
        }
    }
    function deleteExpenseCategory(Request $request){
        $expense_category = $request->expense_category;
        // return $supplier_id;
        try{
            ExpenseCategory::where('id', $expense_category)->delete();
            return redirect()->back()->with('success', 'Expense Category was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Expense Category was not Deleted');
        }
    }
}
