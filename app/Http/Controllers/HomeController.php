<?php

namespace App\Http\Controllers;

use App\Models\Grn;
use App\Models\Item;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = Item::all()->count();
        $totalPurchases = number_format(Grn::all()->sum('amount'));
        $totalSales = number_format(Sale::all()->sum('amount'));

        $sumofa = DB::table('sale_items')
            ->leftJoin('sales','sale_items.sale_id','=','sales.id')
            ->leftJoin('item_stocks','item_stocks.id','=','sale_items.item_stock_id')
            ->leftJoin('items','items.id','=','item_stocks.item_id')
            ->select(DB::raw('sum(sale_items.amount * sale_items.quantity) as sumofsales'))
            ->get();

        $totalBuyings = DB::table('sale_items')
            ->leftJoin('item_stocks','sale_items.item_stock_id', '=','item_stocks.id')
            ->select(DB::raw('sum(item_stocks.buying_price * sale_items.quantity) as sum_buying_price'))
            ->get();

        $profit_loss = number_format(Sale::all()->sum('amount')-$totalBuyings[0]->sum_buying_price);

        return view('dashboard', [
            'totalPurchases' => $totalPurchases,
            'totalSales'=>$totalSales,
            'profit_loss'=>$profit_loss,
            'buying_price'=>number_format($totalBuyings[0]->sum_buying_price),
            'items'=>$items
        ]);
    }
}
