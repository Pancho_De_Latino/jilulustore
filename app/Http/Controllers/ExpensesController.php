<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\ExpenseCategory;
use App\Models\Item;
use App\Models\UnitOfMeasure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        $expenses = Expense::with([
                'expense_category_details'
            ])
            ->orderByDesc('created_at')
            ->get();
        $expense_categories = ExpenseCategory::all();

        // return $expenses;
        return view('expenses', [
            'expenses' => $expenses,
            'expense_categories' => $expense_categories,
        ]);
    }
    function create_new_expense(Request $request){
        // return $request;
        $this->validate($request,[
            'expense_category'=>['required'],
            'details'=>['required'],
            'amount'=>['required']
        ]);
        try{
            $expenses=new Expense();
            $expenses->expense_category=$request->expense_category;
            $expenses->details=$request->details;
            $expenses->amount=$request->amount;
            $expenses->created_by=Auth::user()->id;
            $expenses->save();
            return redirect()->back()->with('success', 'Expense was successfully created');
        }catch(Exception $e){
            return redirect()->back()->withInput()->with('error', 'Expense was not Saved');
        }
    }
}
