<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    function changePassword(Request $request){

        //$credentials = $request->only('user_name', 'password');
        $prev_password = $request->prev_password;
        $user_name = Auth::user()->user_name;
        $password = $request->new_password;

        $validator = Validator::make($request->all(),
            ['prev_password'=>'required',
            'new_password'=>'required',
            'conf_password'=>'required_with:new_password|same:new_password|min:6']
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        if (Auth::attempt(['user_name' => $user_name, 'password' => $prev_password])) {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($password);
            $user->save();
            Auth::logout();
            return redirect()->route('login')->with('success', 'Password was successfully changed');
            // }

            //return redirect()->route('welcome');
        } else {
            return redirect()->back()->with('error', 'Password was not changed');
        }
    }
}
