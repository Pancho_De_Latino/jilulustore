<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\UnitOfMeasure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        $items = Item::with([
            'item_uom','item_created_by'])
            ->orderByDesc('created_at')
            ->get();
        $uoms = UnitOfMeasure::all();

        // return $items;
        return view('items', [
            'items' => $items,
            'uoms'=>$uoms,
        ]);
    }
    function create_new_item(Request $request){
        $this->validate($request,[
            'item_name'=>['required'],
            'sale_type'=>['required'],
            'uom'=>['required']
        ]);
        if($request->sale_type=="1"){
            $request->sale_type=True;
        }else {
            $request->sale_type=False;
        }
        try{
            $item=new Item();
            $item->item_name=$request->item_name;
            $item->unit_of_measure=$request->uom;
            $item->is_retail_sale=$request->sale_type;
            $item->created_by=Auth::user()->id;
            $item->save();
            return redirect()->back()->with('success', 'Item was successfully created');
        }catch(Exception $e){
            return redirect()->back()->withInput()->with('error', 'Item was not Saved');
        }




    }
}
