<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemStock;
use App\Models\Profoma;
use App\Models\ProfomaItem;
use App\Models\Sale;
use App\Models\SaleItem;
use App\Models\SaleStatus;
use App\Models\Supplier;
use App\Models\UnitOfMeasure;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;

class SaleController extends Controller
{
    function open_new_sale(){
        $items = Item::with([
            'item_current_stock',
            'item_uom'
            ])
            ->whereHas('item_current_stock', function(Builder $quey){
                $quey->where('quantity','>',0);
            },)
            ->orderBy('item_name')
            ->get();

        return view('createnewsale', [
            'items' => $items,
        ]);

    }
    function generateReceiptNumber() {
        $now = date_create(date("Y-m-d"));
        $number=date_format($now,"dmY");
        $countTodaysSales = Sale::whereDate('created_at','=',$now)->count();
        $countTodaysSales++;
        $receiptnumber="REC".$number.$countTodaysSales;
        while(DB::table('sales')->where('receipt_number', $receiptnumber)->exists()){
            $receiptnumber = $this->generateReceiptNumber();
        }
        return $receiptnumber;
    }
    function generateProfomaNumber() {
        $now = date_create(date("Y-m-d"));
        $number=date_format($now,"dmY");
        $countTodaysProfomas = Profoma::whereDate('created_at','=',$now)->count();
        $countTodaysProfomas++;
        $profomanumber="INV".$number.$countTodaysProfomas;
        while(DB::table('profomas')->where('profoma_number', $profomanumber)->exists()){
            $profomanumber = $this->generateProfomaNumber();
        }
        return $profomanumber;
    }
    function create_new_sale(Request $request){
        $this->validate($request,[
            'customer_name'=>['required']
        ]);
        $customer_name = $request->customer_name;
        $platform = $request->platform;
        $sale_type=$request->sale_type;
        $itemid= array();
        $quantity=array();
        $printedquantity=array();
        $mrp=array();
        $uom_quantity=array();
        $actual_quantity=array();

        foreach ($request->all() as $key=>$value){
            if(substr($key,0,6)==='itemid'){ $itemid[]=$value; }
            if(substr($key,0,8)==='quantity'){ $quantity[]=$value; }
            if(substr($key,0,8)==='quantity'){ $printedquantity[]=$value; }
            if(substr($key,0,3)==='mrp'){ $mrp[]=$value; }
            if(substr($key,0,12)==='uom_quantity'){ $uom_quantity[]=$value; }
            if(substr($key,0,15)==='actual_quantity'){ $actual_quantity[]=$value; }
        }
        $totalmrp=array();
        $grandtotal=0;
        for($i=0; $i<count($quantity); $i++){
            $totalmrp[$i]=$quantity[$i]*$mrp[$i];
            $grandtotal+=$totalmrp[$i];
        }
        if($sale_type=="1"){
            $sale_type=True;
        } else {
            $sale_type=False;
        }
        if($platform=="0"){
            $platform=True;
        } else {
            $platform=False;
        }
        if($platform){
            try{
                DB::beginTransaction();
                $items = Item::with([
                    'item_current_stock' => function($quey){
                        $quey->orderByDesc('expire_date');
                    },
                    'item_uom'
                    ])
                    ->whereHas('item_current_stock', function(Builder $quey){
                        $quey->where('quantity','>',0);
                    },)
                    ->get();

                $sale=new Sale();
                $sale->customer=$customer_name;
                $sale->is_cash=$sale_type;
                $sale->receipt_number=$this->generateReceiptNumber();
                $sale->sale_status = SaleStatus::where('status_name','=','created')->first()->id;
                $sale->amount=$grandtotal;
                $sale->created_by = Auth::user()->id;
                $sale->save();
                for($i=0;$i<count($itemid);$i++){
                    if($items->find($itemid[$i])->item_current_stock->sum('quantity')>=$quantity[$i]){
                        $item_stocks = $items->find($itemid[$i])->item_current_stock;
                        //here its tomorrows task
                        foreach($item_stocks as $item_stock){
                            $stock = ItemStock::find($item_stock->id);

                            if($quantity[$i]>0){
                                if($stock->quantity >= $quantity[$i]){
                                    $stock->quantity=$stock->quantity-$quantity[$i];
                                    $stock->actual_quantity=$stock->actual_quantity-$actual_quantity[$i];
                                    $stock->save();

                                    //create a model that receives those from quantity[]
                                    $saleitem=new SaleItem();
                                    $saleitem->sale_id=$sale->id;
                                    $saleitem->item_stock_id=$stock->id;
                                    $saleitem->quantity=$quantity[$i];
                                    $saleitem->amount=$mrp[$i];
                                    $saleitem->actual_quantity=$actual_quantity[$i];
                                    $saleitem->save();

                                    $quantity[$i] = 0;
                                    $actual_quantity[$i] = 0; //current number

                                } else if ($stock->quantity < $quantity[$i]){
                                    $quantity[$i] = $quantity[$i] - $stock->quantity;
                                    $actual_quantity[$i] = $actual_quantity[$i] - $stock->actual_quantity;

                                    //create a model that receives those from $stock->quantity
                                    $saleitem=new SaleItem();
                                    $saleitem->sale_id=$sale->id;
                                    $saleitem->item_stock_id=$stock->id;
                                    $saleitem->quantity=$stock->quantity;
                                    $saleitem->amount=$mrp[$i];
                                    $saleitem->actual_quantity=$stock->actual_quantity;
                                    $saleitem->save();

                                    $stock->quantity=0;
                                    $stock->actual_quantity=0;
                                    $stock->save();
                                }
                            }
                        }
                    }else{
                        return redirect()->back()->withInput()->with('error', 'Sale Transaction was not Saved');
                    }
                }
                DB::commit();

                $allitems=array();
                for($i=0;$i<count($itemid);$i++){
                    $allitems[] = (new InvoiceItem())->title(Item::find($itemid[$i])->item_name)->pricePerUnit($mrp[$i])->quantity($printedquantity[$i])->units(UnitOfMeasure::find(Item::find($itemid[$i])->unit_of_measure)->uom_short_name);
                }
                $client = new Party([
                    'name'          => Auth::user()->first_names ." ". Auth::user()->last_name,
                ]);

                $customer = new Party([
                    'name'          => $customer_name,
                ]);

                $notes = [
                    'Available at Chanika Dar es salaam, Tanzania',
                ];
                $notes = implode("<br>", $notes);

                $invoice = Invoice::make('receipt')
                    ->series($sale->receipt_number)
                    // ability to include translated invoice status
                    // in case it was paid
                    ->serialNumberFormat('{SERIES}')
                    ->seller($client)
                    ->buyer($customer)
                    ->date(now("EAT"))
                    ->dateFormat('d/m/Y H:m:s')
                    ->currencySymbol('/-')
                    ->currencyCode('TSH')
                    ->currencyFormat('{VALUE}{SYMBOL}')
                    ->currencyThousandsSeparator(',')
                    ->currencyDecimalPoint('.')
                    ->filename($sale->receipt_number)
                    ->addItems($allitems)
                    ->notes($notes)
                    ->logo(public_path('vendor/invoices/sample-logo.JPG'))
                    // You can additionally save generated invoice to configured disk
                    ->save('public');

                $link = $invoice->url();
                // Then send email to party with link

                // And return invoice itself to browser or have a different view
                $invoice->stream();
                $filename=$sale->receipt_number.".pdf";
                return redirect(asset(Storage::url('public/'.$filename.'')));
            }catch(Exception $e){
                return redirect()->back()->withInput()->with('error', 'Sale Transaction was not Saved');
            }
        } else {
            DB::beginTransaction();
            $items = Item::with([
                'item_current_stock' => function($quey){
                    $quey->orderByDesc('expire_date');
                },
                'item_uom'
                ])
                ->whereHas('item_current_stock', function(Builder $quey){
                    $quey->where('quantity','>',0);
                },)
                ->get();

            $profoma=new Profoma();
            $profoma->customer=$customer_name;
            $profoma->profoma_number=$this->generateProfomaNumber();
            $profoma->amount=$grandtotal;
            $profoma->created_by = Auth::user()->id;
            $profoma->save();
            for($i=0;$i<count($itemid);$i++){
                if($items->find($itemid[$i])->item_current_stock->sum('quantity')>=0){
                    $item_stocks = $items->find($itemid[$i])->item_current_stock;
                    //here its tomorrows task
                    foreach($item_stocks as $item_stock){
                        $stock = ItemStock::find($item_stock->id);

                        //create a model that receives those from quantity[]
                        $profomaitem=new ProfomaItem();
                        $profomaitem->profoma_id=$profoma->id;
                        $profomaitem->item_stock_id=$stock->id;
                        $profomaitem->quantity=$quantity[$i];
                        $profomaitem->amount=$mrp[$i];
                        $profomaitem->actual_quantity=$actual_quantity[$i];
                        $profomaitem->save();
                    }
                }else{
                    return redirect()->back()->withInput()->with('error', 'Sale Transaction was not Saved');
                }
            }
            DB::commit();

            $allitems=array();
            for($i=0;$i<count($itemid);$i++){
                $allitems[] = (new InvoiceItem())->title(Item::find($itemid[$i])->item_name)->pricePerUnit($mrp[$i])->quantity($printedquantity[$i])->units(UnitOfMeasure::find(Item::find($itemid[$i])->unit_of_measure)->uom_short_name);
            }
            $client = new Party([
                'name'          => Auth::user()->first_names ." ". Auth::user()->last_name,
            ]);

            $customer = new Party([
                'name'          => $customer_name,
            ]);

            $notes = [
                'Available at Chanika Dar es salaam, Tanzania',
            ];
            $notes = implode("<br>", $notes);

            $invoice = Invoice::make('profoma')
                ->series($profoma->profoma_number)
                // ability to include translated invoice status
                // in case it was paid
                ->serialNumberFormat('{SERIES}')
                ->seller($client)
                ->buyer($customer)
                ->date(now("EAT"))
                ->dateFormat('d/m/Y H:m:s')
                ->currencySymbol('/-')
                ->currencyCode('TSH')
                ->currencyFormat('{VALUE}{SYMBOL}')
                ->currencyThousandsSeparator(',')
                ->currencyDecimalPoint('.')
                ->filename($profoma->profoma_number)
                ->addItems($allitems)
                ->notes($notes)
                ->logo(public_path('vendor/invoices/sample-logo.JPG'))
                // You can additionally save generated invoice to configured disk
                ->save('public');

            $link = $invoice->url();
            // Then send email to party with link

            // And return invoice itself to browser or have a different view
            $invoice->stream();
            $filename=$profoma->profoma_number.".pdf";
            return redirect(asset(Storage::url('public/'.$filename.'')));
            return redirect()->back()->with('success', 'Proforma was successfully Created');
        }
    }
    function view_sale(){
        $saleitems = SaleItem::with([
            'sale_details',
            'item_stock_details'
            ])
            ->orderByDesc('created_at')
            ->get();
        // Item::find($saleitems[1]->item_stock_details->item_id)->item_name;
        $cashSale = Sale::where('is_cash','=',True)->count();
        $creditSale = Sale::where('is_cash','=',False)->count();
        $totalSale = $cashSale + $creditSale;
        // return UnitOfMeasure::find(Item::find($saleitems[7]->item_stock_details->item_id)->unit_of_measure)->uom_short_name;
        return view('viewsale', [
            'saleitems' => $saleitems,
            'cashSale' => $cashSale,
            'creditSale' => $creditSale,
            'totalSale'=>$totalSale
        ]);
    }
    function view_onesale($sale_id){
        $sale = Sale::where('id','=',$sale_id)->first();
        $sale_items = DB::table('sale_items')
            ->leftJoin('item_stocks','item_stocks.id', '=','sale_items.item_stock_id')
            ->leftJoin('items','items.id', '=','item_stocks.item_id')
            ->leftJoin('unit_of_measures','unit_of_measures.id', '=','items.unit_of_measure')
            ->select('items.*','sale_items.*','unit_of_measures.*')
            ->where('sale_items.sale_id','=',$sale_id)
            ->get();
        // return $grn;

        $allitems = array();
        for ($i = 0; $i < count($sale_items); $i++) {
            $allitems[] = (new InvoiceItem())->title($sale_items[$i]->item_name)->pricePerUnit($sale_items[$i]->amount)->quantity($sale_items[$i]->quantity)->units($sale_items[$i]->uom_short_name);
        }
        $client = new Party([
            'name' => User::find($sale->created_by)->first_names . " " . User::find($sale->created_by)->last_name,
        ]);

        $customer = new Party([
            'name' => $sale->customer,
        ]);

        $notes = [
            'Available at Chanika Dar es salaam, Tanzania',
        ];
        $notes = implode("<br>", $notes);

        $invoice = Invoice::make('RECEIPT')
            ->series($sale->receipt_number)
            // ability to include translated invoice status
            // in case it was paid
            ->serialNumberFormat('{SERIES}')
            ->seller($client)
            ->buyer($customer)
            ->date($sale->created_at)
            ->dateFormat('d/m/Y H:m:s')
            ->currencySymbol('/-')
            ->currencyCode('TSH')
            ->currencyFormat('{VALUE}{SYMBOL}')
            ->currencyThousandsSeparator(',')
            ->currencyDecimalPoint('.')
            ->filename($sale->receipt_number)
            ->addItems($allitems)
            ->notes($notes)
            ->logo(public_path('vendor/invoices/sample-logo.JPG'))
            // You can additionally save generated invoice to configured disk
            ->save('public');

        $link = $invoice->url();
        // Then send email to party with link

        // And return invoice itself to browser or have a different view
        $invoice->stream();
        $filename = $sale->receipt_number . ".pdf";
        return redirect(asset(Storage::url('public/'.$filename.'')));
    }
}
