<?php

namespace App\Http\Controllers;

use App\Exports\ProfitLossExport;
use App\Exports\StockExport;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\UnitOfMeasure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(){
        $items = Item::with([
            'item_uom','item_created_by'])
            ->orderBy('item_name')
            ->get();
        $uoms = UnitOfMeasure::all();
        return view('reports', [
            'items' => $items,
            'uoms'=>$uoms,
        ]);
    }
    function exportProfitOrLossReport(Request $request){
        // return $request;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $items = $request->items == null ? Array() : $request->items;
        $expenses = $request->expenses == null ? "" : $request->expenses;

        return (new ProfitLossExport($from_date,$to_date,$items,$expenses))->download('profitorloss.xlsx');
    }
    function exportStockReport(Request $request){
        $items = $request->items == null ? Item::select('id')->get()->toArray() : $request->items;

        return (new StockExport($items))->download('currentstock.xlsx');
    }
}
