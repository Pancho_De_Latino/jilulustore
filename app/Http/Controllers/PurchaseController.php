<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Grn;
use App\Models\GrnItem;
use App\Models\GrnStatus;
use App\Models\Item;
use App\Models\ItemStock;
use App\Models\PoStatus;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use App\Models\Supplier;
use App\User;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;
use LaravelDaily\Invoices\Facades\Invoice;
use phpDocumentor\Reflection\PseudoTypes\False_;

class PurchaseController extends Controller
{
    function open_new_purchase()
    {
        $now = Carbon::now()->toDateString();
        $suppliers = Supplier::all();
        $items = Item::with([
            'item_uom',
            'item_grns' => function ($quey) {
                $quey->orderByDesc('created_at');
            }
        ])
            ->get();

        return view('createnewpurchase', [
            'items' => $items,
            'today' => $now,
            'suppliers' => $suppliers
        ]);
    }
    function generatePONumber()
    {
        $now = date_create(date("Y-m-d"));
        $number = date_format($now, "dmY");
        $countTodaysPO = PurchaseOrder::whereDate('created_at', '=', $now)->count();
        $countTodaysPO++;
        $ponumber = "PO" . $number . $countTodaysPO;
        while (DB::table('purchase_orders')->where('order_number', $ponumber)->exists()) {
            $ponumber = $this->generatePONumber();
        }
        return $ponumber;
    }
    function generateGRNNumber()
    {
        $now = date_create(date("Y-m-d"));
        $number = date_format($now, "dmY");
        $countTodaysGRN = Grn::whereDate('created_at', '=', $now)->count();
        $countTodaysGRN++;
        $grnnumber = "GRN" . $number . $countTodaysGRN;
        while (DB::table('grns')->where('grn_number', $grnnumber)->exists()) {
            $grnnumber = $this->generateGRNNumber();
        }
        return $grnnumber;
    }
    function create_new_purchase(Request $request)
    {
        $this->validate($request, [
            'supplier' => ['required']
        ]);
        $supplier = $request->supplier;
        $purchase_type = $request->purchase_type;
        $item = array();
        $quantity = array();
        $batchno = array();
        $expiredate = array();
        $buyingprice = array();
        $mrp = array();

        foreach ($request->all() as $key => $value) {
            if (substr($key, 0, 6) === 'select') {
                $item[] = $value;
            }
            if (substr($key, 0, 8) === 'quantity') {
                $quantity[] = $value;
            }
            if (substr($key, 0, 7) === 'batchno') {
                $batchno[] = $value;
            }
            if (substr($key, 0, 10) === 'expiredate') {
                $expiredate[] = $value;
            }
            if (substr($key, 0, 11) === 'buyingprice') {
                $buyingprice[] = $value;
            }
            if (substr($key, 0, 3) === 'mrp') {
                $mrp[] = $value;
            }
        }
        $totalbuyingprice = array();
        $grandtotal = 0;
        for ($i = 0; $i < count($quantity); $i++) {
            $totalbuyingprice[$i] = $quantity[$i] * $buyingprice[$i];
            $grandtotal += $totalbuyingprice[$i];
        }

        if ($purchase_type === 'po') {
            try {
                DB::beginTransaction();
                $purchaseorder = new PurchaseOrder();
                $purchaseorder->order_number = $this->generatePONumber();
                $purchaseorder->po_status = PoStatus::where('status_name', '=', 'created')->first()->id;
                $purchaseorder->amount = $grandtotal;
                $purchaseorder->supplier_id = $supplier;
                $purchaseorder->created_by = Auth::user()->id;
                $purchaseorder->save();

                for ($i = 0; $i < count($quantity); $i++) {
                    $item_uom_qty = Item::with(['item_uom'])->where('id', '=', $item[0])->get();
                    $item_uom_quantity = $item_uom_qty[0]->item_uom->uom_quantity;
                    $purchaseorderitem = new PurchaseOrderItem();
                    $purchaseorderitem->order_id = $purchaseorder->id;
                    $purchaseorderitem->item_id = $item[$i];
                    $purchaseorderitem->quantity = $quantity[$i];
                    $purchaseorderitem->buying_price = $buyingprice[$i];
                    $purchaseorderitem->selling_price = $mrp[$i];
                    $purchaseorderitem->expire_date = $expiredate[$i];
                    $purchaseorderitem->actual_quantity = $item_uom_quantity * $quantity[$i];
                    $purchaseorderitem->actual_buying_price = $buyingprice[$i] / $item_uom_quantity;
                    $purchaseorderitem->actual_selling_price = $mrp[$i] / $item_uom_quantity;
                    $purchaseorderitem->save();
                }

                DB::commit();

                return redirect()->back()->with('success', 'Purchase Order Has Successfully Saved');
            } catch (Exception $e) {
                return redirect()->back()->withInput()->with('error', 'Purchase Order was not Saved');
            }
        } else {
            try {
                if ($purchase_type == "1") {
                    $purchase_type = True;
                } else {
                    $purchase_type = False;
                }
                DB::beginTransaction();
                $grn = new Grn();
                $grn->grn_number = $this->generateGRNNumber();
                $grn->grn_status = GrnStatus::where('status_name', '=', 'created')->first()->id;
                $grn->is_cash = $purchase_type;
                $grn->amount = $grandtotal;
                $grn->supplier_id = $supplier;
                $grn->created_by = Auth::user()->id;
                $grn->save();

                for ($i = 0; $i < count($quantity); $i++) {
                    $item_uom_qty = Item::with(['item_uom'])->where('id', '=', $item[0])->get();
                    $item_uom_quantity = $item_uom_qty[0]->item_uom->uom_quantity;
                    $grnitem = new GrnItem();
                    $grnitem->grn_id = $grn->id;
                    $grnitem->item_id = $item[$i];
                    $grnitem->quantity = $quantity[$i];
                    $grnitem->buying_price = $buyingprice[$i];
                    $grnitem->selling_price = $mrp[$i];
                    $grnitem->expire_date = $expiredate[$i];
                    $grnitem->batch_number = $batchno[$i];
                    $grnitem->actual_quantity = $item_uom_quantity * $quantity[$i];
                    $grnitem->actual_buying_price = $buyingprice[$i] / $item_uom_quantity;
                    $grnitem->actual_selling_price = $mrp[$i] / $item_uom_quantity;
                    $grnitem->save();

                    $itemstock = new ItemStock();
                    $itemstock->item_id = $item[$i];
                    $itemstock->quantity = $quantity[$i];
                    $itemstock->grn_id = $grn->id;
                    $itemstock->buying_price = $buyingprice[$i];
                    $itemstock->selling_price = $mrp[$i];
                    $itemstock->expire_date = $expiredate[$i];
                    $itemstock->actual_quantity = $item_uom_quantity * $quantity[$i];
                    $itemstock->actual_buying_price = $buyingprice[$i] / $item_uom_quantity;
                    $itemstock->actual_selling_price = $mrp[$i] / $item_uom_quantity;
                    $itemstock->save();
                }
                DB::commit();

                return redirect()->back()->with('success', 'GRN Has Successfully Saved');
            } catch (Exception $e) {
                return redirect()->back()->withInput()->with('error', 'GRN was not Saved');
            }
        }
    }
    function view_purchase_order()
    {
        $purchase_orders = PurchaseOrder::with([
            'po_supplier', 'po_current_status', 'po_created_by'
        ])
            ->orderByDesc('created_at')
            ->get();

        $creditGRN = Grn::where('is_cash', '=', True)->count();
        $cashGRN = Grn::where('is_cash', '=', False)->count();
        $po = PurchaseOrder::count();

        return view('viewpurchaseorder', [
            'purchase_orders' => $purchase_orders,
            'creditGRN' => $creditGRN,
            'cashGRN' => $cashGRN,
            'po' => $po
        ]);
    }
    function view_grns()
    {
        $grns = Grn::with([
            'grn_supplier', 'grn_current_status', 'grn_created_by'
        ])
            ->orderByDesc('created_at')
            ->get();

        $creditGRN = Grn::where('is_cash', '=', True)->count();
        $cashGRN = Grn::where('is_cash', '=', False)->count();
        $po = PurchaseOrder::count();

        return view('viewgrns', [
            'grns' => $grns,
            'creditGRN' => $creditGRN,
            'cashGRN' => $cashGRN,
            'po' => $po
        ]);
    }
    function view_grn($grn_id)
    {
        // return $id;
        $grn = Grn::where('id','=',$grn_id)->first();
        // return $grn;
        $grn_items = DB::table('grn_items')
            ->leftJoin('items','items.id', '=','grn_items.item_id')
            ->leftJoin('unit_of_measures','unit_of_measures.id', '=','items.unit_of_measure')
            ->select('items.*','grn_items.*','unit_of_measures.*')
            ->where('grn_items.grn_id','=',$grn_id)
            ->get();

        $allitems = array();
        for ($i = 0; $i < count($grn_items); $i++) {
            $allitems[] = (new InvoiceItem())->title($grn_items[$i]->item_name)->pricePerUnit($grn_items[$i]->buying_price)->quantity($grn_items[$i]->quantity)->units($grn_items[$i]->uom_short_name);
        }
        $client = new Party([
            'name' => User::find($grn->created_by)->first_names . " " . User::find($grn->created_by)->last_name,
        ]);

        $customer = new Party([
            'name' => Supplier::find($grn->supplier_id)->supplier_name,
        ]);

        $notes = [
            'Available at Chanika Dar es salaam, Tanzania',
        ];
        $notes = implode("<br>", $notes);
        // return $grn;
        $invoice = Invoice::make('GRN')
            ->series($grn->grn_number)
            // ability to include translated invoice status
            // in case it was paid
            ->serialNumberFormat('{SERIES}')
            ->seller($client)
            ->buyer($customer)
            ->date($grn->created_at)
            ->dateFormat('d/m/Y H:m:s')
            ->currencySymbol('/-')
            ->currencyCode('TSH')
            ->currencyFormat('{VALUE}{SYMBOL}')
            ->currencyThousandsSeparator(',')
            ->currencyDecimalPoint('.')
            ->filename($grn->grn_number)
            ->addItems($allitems)
            ->notes($notes)
            ->logo(public_path('vendor/invoices/sample-logo.JPG'))
            // You can additionally save generated invoice to configured disk
            ->save('public');

        $link = $invoice->url();
        // Then send email to party with link

        // And return invoice itself to browser or have a different view
        $invoice->stream();
        $filename = $grn->grn_number . ".pdf";
        return redirect(asset(Storage::url('public/'.$filename.'')));
    }
}
