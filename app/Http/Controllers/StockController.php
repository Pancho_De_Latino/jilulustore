<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    function index()
    {
        $items = Item::with([
            'item_current_stock' => function ($quey) {
                $quey->where('quantity', '>=', 0);
            },
            'item_uom'
            ])
            ->orderBy('item_name')
            ->get();
        // return $items;

        $lessthan10 = Item::with([
            'item_current_stock',
            'item_uom'
            ])
            ->whereHas('item_current_stock', function (Builder $quey) {
                $quey->select(DB::raw('sum(quantity) as sum_quantity'))->having('sum_quantity', '>', 0)->having('sum_quantity', '<=', 10);
            },)
            ->orderBy('item_name')
            ->get()->count();

        $greaterthan10 = Item::with([
            'item_current_stock',
            'item_uom'
            ])
            ->whereHas('item_current_stock', function (Builder $quey) {
                $quey->select(DB::raw('sum(quantity) as sum_quantity'))->having('sum_quantity', '>', 10);
            },)
            ->orderBy('item_name')
            ->get()->count();

        $zeros = $items->count() - ($lessthan10 + $greaterthan10);

        return view('viewstock', [
            'items' => $items,
            'outofstock' => $zeros,
            'lessthan10' => $lessthan10,
            'greaterthan10' => $greaterthan10,
        ]);
    }
    function itemTracking($item_id){
        // return $item_id;
        $itemname=Item::find($item_id)->item_name;
        $a = DB::table('sale_items')
            ->leftJoin('sales','sale_items.sale_id','=','sales.id')
            ->leftJoin('item_stocks','item_stocks.id','=','sale_items.item_stock_id')
            ->leftJoin('items','items.id','=','item_stocks.item_id')
            ->select('items.*','sale_items.quantity','sales.receipt_number',DB::raw('sales.created_at as transactiondate'),'sales.customer',DB::raw('"a"'))
            ->where('item_stocks.item_id','=',$item_id);

        $b = DB::table('grn_items')
            ->leftJoin('grns','grn_items.grn_id','=','grns.id')
            ->leftJoin('suppliers','suppliers.id','=','grns.supplier_id')
            ->leftJoin('items','items.id','=','grn_items.item_id')
            ->select('items.*','grn_items.quantity','grns.grn_number',DB::raw('grns.created_at as transactiondate'),'suppliers.supplier_name',DB::raw('"b"'))
            ->where('items.id','=',$item_id);
        $itemmovement = $a->union($b)->orderBy('transactiondate')->get();
        // return $itemmovement;
        return view('itemmovement', [
            'itemmovement' => $itemmovement,
            'itemname' => $itemname,
        ]);
    }
}
