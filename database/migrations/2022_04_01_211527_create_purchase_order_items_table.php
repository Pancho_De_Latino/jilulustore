<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('order_id');
            $table->uuid('item_id');
            $table->double('quantity');
            $table->double('buying_price');
            $table->double('selling_price');
            $table->date('expire_date');
            $table->unsignedBigInteger('actual_quantity');
            $table->double('actual_buying_price');
            $table->double('actual_selling_price');
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('purchase_orders')
                ->onDelete('restrict');

            $table->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_items');
    }
}
