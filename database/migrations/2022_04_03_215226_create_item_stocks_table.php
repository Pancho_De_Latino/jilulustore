<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stocks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('item_id');
            $table->uuid('grn_id');
            $table->double('quantity');
            $table->double('buying_price');
            $table->double('selling_price');
            $table->date('expire_date');
            $table->unsignedBigInteger('actual_quantity');
            $table->double('actual_buying_price');
            $table->double('actual_selling_price');
            $table->timestamps();
            $table->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('restrict');
            $table->foreign('grn_id')
                ->references('id')
                ->on('grns')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stocks');
    }
}
