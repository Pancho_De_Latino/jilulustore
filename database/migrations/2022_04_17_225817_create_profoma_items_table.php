<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfomaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profoma_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('profoma_id');
            $table->uuid('item_stock_id');
            $table->double('quantity');
            $table->double('amount');
            $table->unsignedBigInteger('actual_quantity');
            $table->timestamps();

            $table->foreign('profoma_id')
                ->references('id')
                ->on('profomas')
                ->onDelete('restrict');

            $table->foreign('item_stock_id')
                ->references('id')
                ->on('item_stocks')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profoma_items');
    }
}
