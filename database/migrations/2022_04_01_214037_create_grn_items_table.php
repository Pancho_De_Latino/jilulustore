<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('grn_id');
            $table->uuid('item_id');
            $table->double('quantity');
            $table->double('buying_price');
            $table->double('selling_price');
            $table->date('expire_date');
            $table->string('batch_number');
            $table->unsignedBigInteger('actual_quantity');
            $table->double('actual_buying_price');
            $table->double('actual_selling_price');
            $table->timestamps();

            $table->foreign('grn_id')
                ->references('id')
                ->on('grns')
                ->onDelete('restrict');

            $table->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_items');
    }
}
