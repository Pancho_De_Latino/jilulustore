<?php

use App\Models\Grn;
use App\Models\GrnItem;
use App\Models\GrnStatus;
use App\Models\Item;
use App\Models\PoStatus;
use App\Models\SaleStatus;
use App\Models\Supplier;
use Illuminate\Database\Seeder;
use App\User;
use App\Models\UnitOfMeasure;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $adminUser = User::create([
            'user_name' => 'lucas.jilulu',
            'first_names' => 'Lucas',
            'last_name' => 'Jilulu',
            'email' => 'lucas.jilulu@yahoo.com',
            'password' => bcrypt('12345'),
            'phone_number' => '0627035174',
            'is_active' => true,
        ]);
        $admin = Role::create(['name' => 'admin']);
        $customrole = Role::create(['name' => 'custom role']);

        $createuser = Permission::create(['name' => 'create user']);
        $viewusers = Permission::create(['name' => 'view users']);
        $deactivateuser = Permission::create(['name' => 'deactivate user']);
        $edituser = Permission::create(['name' => 'edit user']);

        $viewtotalsales = Permission::create(['name' => 'view total sales']);
        $viewtotalbuyingprice = Permission::create(['name' => 'view total buying price']);
        $viewprofit = Permission::create(['name' => 'view profit']);

        $createnewpurchase = Permission::create(['name' => 'create new purchase']);
        $viewpurchaseorders = Permission::create(['name' => 'view purchase orders']);
        $viewgrns = Permission::create(['name' => 'view grns']);
        $printgrn = Permission::create(['name' => 'print grn']);

        $createnewsale = Permission::create(['name' => 'create new sale']);
        $viewsales = Permission::create(['name' => 'view sales']);
        $printreceipt = Permission::create(['name' => 'print receipt']);
        $printgrnfromsales = Permission::create(['name' => 'print grn from sales']);

        $createnewitem = Permission::create(['name' => 'create new item']);
        $viewitems = Permission::create(['name' => 'view items']);

        $viewstock = Permission::create(['name' => 'view stock']);
        $viewitemmovement = Permission::create(['name' => 'view item movement']);

        $viewexpenses = Permission::create(['name' => 'view expenses']);
        $createexpense = Permission::create(['name' => 'create expense']);

        $profitlossreports = Permission::create(['name' => 'view profit or loss report']);
        $currentstockreport = Permission::create(['name' => 'view current stock report']);

        $createnewuom = Permission::create(['name' => 'create new uom']);
        $viewuom = Permission::create(['name' => 'view uoms']);
        $deleteuom = Permission::create(['name' => 'delete uom']);

        $createnewsupplier = Permission::create(['name' => 'create new supplier']);
        $viewsuppliers = Permission::create(['name' => 'view suppliers']);
        $deletesupplier = Permission::create(['name' => 'delete supplier']);

        $createnewexpensecategory = Permission::create(['name' => 'create new expense category']);
        $viewexpensecategories = Permission::create(['name' => 'view expense categories']);
        $deleteexpensecategory = Permission::create(['name' => 'delete expense category']);

        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'each',
            'uom_short_name'=>'each',
            'uom_quantity'=>1,
            'created_by'=>$adminUser->id,
        ]);

        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'pack of 5',
            'uom_short_name'=>'pack5',
            'uom_quantity'=>5,
            'created_by'=>$adminUser->id,
        ]);
        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'pack of 10',
            'uom_short_name'=>'pack10',
            'uom_quantity'=>10,
            'created_by'=>$adminUser->id,
        ]);
        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'pack of 20',
            'uom_short_name'=>'pack20',
            'uom_quantity'=>20,
            'created_by'=>$adminUser->id,
        ]);
        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'pack of 30',
            'uom_short_name'=>'pack30',
            'uom_quantity'=>30,
            'created_by'=>$adminUser->id,
        ]);

        $unit_of_measure = UnitOfMeasure::create([
            'uom_name'=>'carton',
            'uom_short_name'=>'carton',
            'uom_quantity'=>12,
            'created_by'=>$adminUser->id,
        ]);

        $unit_of_measure1 = UnitOfMeasure::create([
            'uom_name'=>'crate',
            'uom_short_name'=>'crate',
            'uom_quantity'=>24,
            'created_by'=>$adminUser->id,
        ]);

        $item1=Item::create([
            'item_name'=>'Kilimanjaro Beer',
            'unit_of_measure'=>$unit_of_measure1->id,
            'is_retail_sale'=>1,
            'created_by'=>$adminUser->id,
        ]);
        $item2=Item::create([
            'item_name'=>'Serengeti Beer',
            'unit_of_measure'=>$unit_of_measure1->id,
            'is_retail_sale'=>0,
            'created_by'=>$adminUser->id,
        ]);

        $admin->givePermissionTo($createuser);
        $admin->givePermissionTo($viewusers);
        $admin->givePermissionTo($deactivateuser);
        $admin->givePermissionTo($edituser);
        $admin->givePermissionTo($viewtotalsales);
        $admin->givePermissionTo($viewtotalbuyingprice);
        $admin->givePermissionTo($viewprofit);
        $admin->givePermissionTo($createnewpurchase);
        $admin->givePermissionTo($viewpurchaseorders);
        $admin->givePermissionTo($viewgrns);
        $admin->givePermissionTo($printgrn);
        $admin->givePermissionTo($createnewsale);
        $admin->givePermissionTo($viewsales);
        $admin->givePermissionTo($printreceipt);
        $admin->givePermissionTo($printgrnfromsales);
        $admin->givePermissionTo($createnewitem);
        $admin->givePermissionTo($viewitems);
        $admin->givePermissionTo($viewstock);
        $admin->givePermissionTo($viewitemmovement);
        $admin->givePermissionTo($viewexpenses);
        $admin->givePermissionTo($createexpense);
        $admin->givePermissionTo($profitlossreports);
        $admin->givePermissionTo($currentstockreport);
        $admin->givePermissionTo($createnewuom);
        $admin->givePermissionTo($viewuom);
        $admin->givePermissionTo($deleteuom);
        $admin->givePermissionTo($createnewsupplier);
        $admin->givePermissionTo($viewsuppliers);
        $admin->givePermissionTo($deletesupplier);
        $admin->givePermissionTo($createnewexpensecategory);
        $admin->givePermissionTo($viewexpensecategories);
        $admin->givePermissionTo($deleteexpensecategory);

        $adminUser->assignRole($admin);

        $grn_status=GrnStatus::create([
            'status_name'=>'created'
        ]);

        $po_status=PoStatus::create([
            'status_name'=>'created'
        ]);

        $sale_status=SaleStatus::create([
            'status_name'=>'created'
        ]);

        $supplier=Supplier::create([
            'supplier_name'=>'Distributor 1',
            'phone'=>'0659484034',
            'address'=>'P.O. Box 7866 DSM',
            'created_by'=>$adminUser->id
        ]);
        $supplier=Supplier::create([
            'supplier_name'=>'Distributor 2',
            'phone'=>'0659484035',
            'address'=>'P.O. Box 7866 DSM',
            'created_by'=>$adminUser->id
        ]);
        $supplier=Supplier::create([
            'supplier_name'=>'Distributor 3',
            'phone'=>'0659484036',
            'address'=>'P.O. Box 7866 DSM',
            'created_by'=>$adminUser->id
        ]);
        $supplier=Supplier::create([
            'supplier_name'=>'Distributor 4',
            'phone'=>'0659484037',
            'address'=>'P.O. Box 7866 DSM',
            'created_by'=>$adminUser->id
        ]);
        $supplier=Supplier::create([
            'supplier_name'=>'Distributor 5',
            'phone'=>'0659484038',
            'address'=>'P.O. Box 7866 DSM',
            'created_by'=>$adminUser->id
        ]);
    }
}
