<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@login');
Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::post('/login', 'Auth\LoginController@authenticate')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/changepassword', 'Auth\ResetPasswordController@changePassword')->name('changepassword');

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::group(['middleware' => ['can:create new purchase']], function () {
    Route::get('/opencreatepurchase', 'PurchaseController@open_new_purchase')->name('createpurchase');
    Route::post('/savepurchase', 'PurchaseController@create_new_purchase')->name('savepurchase');
});

Route::group(['middleware' => ['can:view purchase orders']], function () {
    Route::get('/viewpurchaseorder', 'PurchaseController@view_purchase_order')->name('viewpurchaseorder');
});

Route::group(['middleware' => ['can:view grns']], function () {
    Route::get('/viewgrns', 'PurchaseController@view_grns')->name('viewgrns');
    Route::get('/viewgrn/{id}', 'PurchaseController@view_grn')->name('viewgrn');
});

Route::group(['middleware' => ['can:create new sale']], function () {
    Route::get('/opencreatesale', 'SaleController@open_new_sale')->name('createsale');
    Route::post('/savesale', 'SaleController@create_new_sale')->name('savesale');
});

Route::group(['middleware' => ['can:view sales']], function () {
    Route::get('/viewsales', 'SaleController@view_sale')->name('viewsales');
    Route::get('/viewonesale/{id}', 'SaleController@view_onesale')->name('viewonesale');
});

Route::group(['middleware' => ['can:view items']], function () {
    Route::get('/items', 'ItemController@index')->name('items');
    Route::post('/createitem', 'ItemController@create_new_item')->name('createitem');
});

Route::group(['middleware' => ['can:create user']], function () {
    Route::get('/createuserpage', 'UserManagementController@createUserPage')->name('createuserpage');
    Route::post('/createuser', 'UserManagementController@createUser')->name('createuser');
});

Route::group(['middleware' => ['can:edit user']], function () {
    Route::get('/edituser', function () {
        return view('admin.edituser');
    })->name('edituser');
    Route::post('/edituserprofile', 'UserManagementController@editUser')->name('edituserprofile');
    Route::post('/edituserpermissions', 'UserManagementController@editUserPermissions')->name('edituserpermissions');
    Route::post('/edituserfromview', 'UserManagementController@editUserFromView')->name('edituserfromview');
});

Route::group(['middleware' => ['can:view users']], function () {
    Route::get('/viewuser', 'UserManagementController@index')->name('viewuser');
});

Route::group(['middleware' => ['can:deactivate user']], function () {
    Route::post('/changestatus', 'UserManagementController@changeStatus')->name('viewuser.changestatus');
});

Route::group(['middleware' => ['permission:create new uom|view uoms|delete uom|create new supplier|view suppliers|delete supplier|create new expense category|view expense categories|delete expense category']], function () {
    Route::get('/settings', 'SettingsController@index')->name('settings');
});

Route::group(['middleware' => ['can:create new uom']], function () {
    Route::post('/settings/uom', 'SettingsController@addUOM')->name('settings.add.uom');
});

Route::group(['middleware' => ['can:create new supplier']], function () {
    Route::post('/settings/supplier', 'SettingsController@addSupplier')->name('settings.add.supplier');
});

Route::group(['middleware' => ['can:create new expense category']], function () {
    Route::post('/settings/expenses', 'SettingsController@addExpenseCategory')->name('settings.add.expenses');
});

Route::group(['middleware' => ['can:delete uom']], function () {
    Route::post('/settings/deleteuom', 'SettingsController@deleteUOM')->name('settings.delete.uom');
});

Route::group(['middleware' => ['can:delete supplier']], function () {
    Route::post('/settings/deletesupplier', 'SettingsController@deleteSupplier')->name('settings.delete.supplier');
});

Route::group(['middleware' => ['can:delete expense category']], function () {
    Route::post('/settings/deleteexpenses', 'SettingsController@deleteExpenseCategory')->name('settings.delete.expensecategory');
});

Route::group(['middleware' => ['permission:view stock|view item movement']], function () {
    Route::get('/viewstock', 'StockController@index')->name('viewstock');
});

Route::group(['middleware' => ['can:view item movement']], function () {
    Route::get('/itemtracking/{id}', 'StockController@itemTracking')->name('itemtracking');
});

Route::group(['middleware' => ['permission:view profit or loss report|view current stock report']], function () {
    Route::get('/reports', 'ReportsController@index')->name('reports');
});

Route::group(['middleware' => ['can:view profit or loss report']], function () {
    Route::post('/profitlossreport', 'ReportsController@exportProfitOrLossReport')->name('profitlossreport');
});

Route::group(['middleware' => ['can:view current stock report']], function () {
    Route::post('/stockreport', 'ReportsController@exportStockReport')->name('stockreport');
});

Route::group(['middleware' => ['permission:view expenses|create expense']], function () {
    Route::get('/expenses', 'ExpensesController@index')->name('expenses');
});

Route::group(['middleware' => ['can:create expense']], function () {
    Route::post('/createexpense', 'ExpensesController@create_new_expense')->name('createexpense');
});
