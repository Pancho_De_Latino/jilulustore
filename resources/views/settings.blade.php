@extends('layouts.app')
@section('title', 'Settings')
@section('content')
    <section class="content-header">
        <h1>Settings</h1>
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Settings</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Unit of measures

                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <form action="{{ route('settings.add.uom') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">UOM name <font color="red">*</font></label>
                                        <input type="text" name="uom_name" value="{{ old('uom_name') }}"
                                            class="form-control" id="exampleInputEmail1" placeholder="Enter UOM name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">UOM short name <font color="red">*</font>
                                        </label>
                                        <input type="text" name="uom_short_name" value="{{ old('uom_short_name') }}"
                                            class="form-control" id="exampleInputPassword1"
                                            placeholder="Enter UOM short name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Quantity <font color="red">*</font></label>
                                        <input type="number" name="uom_quantity" value="{{ old('uom_quantity') }}"
                                            class="form-control" id="exampleInputPassword1"
                                            placeholder="Enter UOM quantity">
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" name="submit" style="align:right;"
                                                class="btn btn-primary">Add</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Abbr</th>
                                            <th>Qty</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($unitofmeasures as $unitofmeasure)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $unitofmeasure->uom_name }}</td>
                                                <td>{{ $unitofmeasure->uom_short_name }}</td>
                                                <td>{{ $unitofmeasure->uom_quantity }}</td>
                                                <td><button type="button" data-toggle="modal"
                                                        data-target="#deleteUOMModal"
                                                        data-unitofmeasure="{{ $unitofmeasure->id }}"
                                                        class="btn btn-danger btn-sm"><i
                                                            class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            Suppliers
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <form action="{{ route('settings.add.supplier') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Supplier name <font color="red">*</font></label>
                                        <input type="text" name="supplier_name" value="{{ old('supplier_name') }}"
                                            class="form-control" id="exampleInputEmail1" placeholder="Enter supplier name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone Number <font color="red">*</font>
                                        </label>
                                        <input type="text" name="phone" value="{{ old('phone') }}"
                                            class="form-control" id="exampleInputPassword1" placeholder="Enter phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Address <font color="red">*</font>
                                        </label>
                                        <input type="text" name="address" value="{{ old('address') }}"
                                            class="form-control" id="exampleInputPassword1" placeholder="Enter P.O. Box..">
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" name="submit" style="align:right;"
                                                class="btn btn-primary">Add</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Abbr</th>
                                            <th>Qty</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($suppliers as $supplier)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $supplier->supplier_name }}</td>
                                                <td>{{ $supplier->phone }}</td>
                                                <td>{{ $supplier->address }}</td>
                                                <td><button type="button" data-toggle="modal"
                                                        data-target="#deleteSupplierModal"
                                                        data-supplier="{{ $supplier->id }}"
                                                        class="btn btn-danger btn-sm"><i
                                                            class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            Expenses
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <form action="{{ route('settings.add.expenses') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Expenses Category Name<font color="red">*</font></label>
                                        <input type="text" name="category_name" value="{{ old('category_name') }}"
                                            class="form-control" placeholder="Enter Category name">
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" name="submit" style="align:right;"
                                                class="btn btn-primary">Add</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($expensecategories as $expensecategory)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $expensecategory->category_name }}</td>
                                                <td><button type="button" data-toggle="modal"
                                                        data-target="#deleteExpenseModal"
                                                        data-expense_category="{{ $expensecategory->id }}"
                                                        class="btn btn-danger btn-sm"><i
                                                            class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
    <!-- /.content -->

    <div class="modal modal-danger fade" id="deleteExpenseModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Expense Category</h4>
                </div>
                <form action="{{ route('settings.delete.expensecategory') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <h5>Are you sure you want to Delete Expense Category?</h5>
                        <input type="hidden" id="expense_category" name="expense_category" />
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline">Yes, Change</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal modal-danger fade" id="deleteUOMModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete UOM</h4>
                </div>
                <form action="{{ route('settings.delete.uom') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <h5>Are you sure you want to Delete UOM?</h5>
                        <input type="hidden" id="unitofmeasure" name="unitofmeasure" />
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline">Yes, Change</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal modal-danger fade" id="deleteSupplierModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Supplier</h4>
                </div>
                <form action="{{ route('settings.delete.supplier') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <h5>Are you sure you want to Delete Supplier?</h5>
                        <input type="hidden" id="supplier_id" name="supplier_id" />
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline">Yes, Change</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('page_scripti')
    <script>
        $('#deleteUOMModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var unitofmeasure = button.data('unitofmeasure')
            var modal = $(this)
            modal.find('#unitofmeasure').val(unitofmeasure)
        })
    </script>
    <script>
        $('#deleteSupplierModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var supplier_id = button.data('supplier')
            var modal = $(this)
            modal.find('#supplier_id').val(supplier_id)
        })
    </script>
    <script>
        $('#deleteExpenseModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var expense_category = button.data('expense_category')
            var modal = $(this)
            modal.find('#expense_category').val(expense_category)
        })
    </script>
@endsection
