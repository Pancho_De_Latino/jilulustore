@extends('layouts.app')
@section('title', 'View GRN')

@section('content')
<section class="content-header">
    <h1>
        View GRN
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View grn</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cash GRN</span>
                <span class="info-box-number">{{$cashGRN}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Credit GRN</span>
                <span class="info-box-number">{{$creditGRN}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Purchase Order</span>
            <span class="info-box-number">{{$po}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>GRN Number</th>
                      <th>Status</th>
                      <th>GRN Type</th>
                      <th>Amount</th>
                      <th>Supplier</th>
                      <th>Created By</th>
                      <th>GRN Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($grns as $grn)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        {{-- {{Route('viewgrn',["id" => $grn->id])}} --}}
                        <td><a href="{{Route('viewgrn',["id" => $grn->id])}}">{{ $grn->grn_number}}</a></td>
                        <td>{{ $grn->grn_current_status->status_name}}</td>
                        <td>
                            @if ($grn->is_cash==1)
                                Cash
                            @else
                                Credit
                            @endif
                        </td>
                        <td>{{ $grn->amount}}</td>
                        <td>{{ $grn->grn_supplier->supplier_name}}</td>
                        <td>{{ $grn->grn_created_by->first_names}} {{ $grn->grn_created_by->last_name}}</td>
                        <td>{{ $grn->created_at}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>GRN Number</th>
                        <th>Status</th>
                        <th>GRN Type</th>
                        <th>Amount</th>
                        <th>Supplier</th>
                        <th>Created By</th>
                        <th>GRN Date</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
    </div>
</section>
  <!-- /.content -->
@endsection


