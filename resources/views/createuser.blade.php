@extends('layouts.app')
@section('title', 'Create User')
@section('content')
<section class="content-header">
    <h1>Create User</h1>
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
        <li class="breadcrumb-item active">Create User</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <form action="{{ route('createuser') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            @include('partial.alert')
        </div>
    </div>
    <div class="row">
    <div class="col-md-6">
        <div class="box">
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">User name <font color="red">*</font></label>
                <input required type="text" class="form-control"  value="{{ old('user_name')}}" name="user_name" id="exampleInputEmail1" placeholder="Enter User ID">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">First names <font color="red">*</font></label>
                <input required type="text" class="form-control"  value="{{ old('first_names')}}" name="first_names" id="exampleInputEmail1" placeholder="Enter First name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Last name <font color="red">*</font></label>
                <input required type="text" class="form-control" value="{{ old('last_name')}}" name="last_name" id="exampleInputEmail1" placeholder="Enter Last name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phone Number <font color="red">*</font></label>
                <input required type="text" class="form-control" value="{{ old('phone_number')}}" name="phone_number" id="exampleInputEmail1" placeholder="Enter Phone number">
            </div>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email <font color="red">*</font></label>
                    <input required type="email" class="form-control" value="{{ old('email')}}" name="email" id="exampleInputEmail1" placeholder="Enter Email">
                </div>
                <div class="form-group">
                    <label>Role <font color="red">*</font></label>
                    <select required name="role" class="form-control select2" style="width: 100%;">
                        <option></option>
                        @foreach ($roles as $role)
                        <option {{ old('role') == $role->id ? "selected" : ""}} value="{{ $role->id }}">{{ $role->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Profile Picture <font color="red">*</font></label>
                    <div class="input-group">
                        <div class="custom-file">
                        <input required type="file"  name="image" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
           <!-- /.card -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <center>
                <button type="submit" class="btn btn-success float-center">
                    <i class="fa fa-plus"></i> Create User
                </button>
            </center>
        </div>
        <div class="col-md-5"></div>
    </div>
    </form>
</section>
@endsection
