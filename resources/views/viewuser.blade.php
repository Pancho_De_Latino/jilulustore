@extends('layouts.app')
@section('title', 'View Users')
@section('content')
    <section class="content-header">
        <h1>View User</h1>
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">View user</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Full Names</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->user_name }}</td>
                                        <td>{{ $user->first_names }} {{ $user->last_name }}</td>
                                        <td>{{ $user->phone_number }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <span data-toggle="modal" data-target="#deleteDepartmentModal"
                                                data-is_active={{ $user->user_id }} data-user_id="{{ $user->user_id }}">
                                                <small class="label pull-center {{ $user->is_active ? 'bg-green' : 'bg-red' }}">
                                                    {{ $user->is_active ? 'active' : 'disabled' }}
                                                </small>
                                            </span>
                                        </td>
                                        <td>{{ $user->name }}</td>
                                        <td style="display: inline;">
                                            <form style="display: inline;" action="{{ route('edituserfromview') }}"
                                                method="post">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{ $user->user_id }}">
                                                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal modal-danger fade" id="deleteDepartmentModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Change Status</h4>
                </div>
                <form action="{{ route('viewuser.changestatus') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <h5>Are you sure you want to Change User Status?</h5>
                        <input type="hidden" id="user_id" name="user_id" />
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline">Yes, Change</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection
@section('page_scripti')
    <script>
        $('#deleteDepartmentModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var user_id = button.data('user_id')
            var modal = $(this)
            modal.find('#user_id').val(user_id)
        })
    </script>
@endsection
