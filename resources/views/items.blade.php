@extends('layouts.app')
@section('title', 'Items')

@section('content')
<section class="content-header">
    <h1>
        Items
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Items</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div>
                <button id="button0" type="button"  onclick="createField()" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus"></i> Add New Item
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('partial.alert')
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Item Name</th>
                      <th>UOM</th>
                      <th>UOM Quantity</th>
                      <th>Sale Type</th>
                      <th>Created By</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->item_name}}</td>
                        <td>{{ $item->item_uom->uom_short_name }}</td>
                        <td>{{ $item->item_uom->uom_quantity }}</td>

                        <td>
                            @if ($item->is_retail_sale==1)
                                Retail
                            @else
                                Whole sale
                            @endif
                        </td>
                        <td>{{ $item->item_created_by->first_names}} {{ $item->item_created_by->last_name}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Item Name</th>
                      <th>UOM</th>
                      <th>UOM Quantity</th>
                      <th>Sale Type</th>
                      <th>Created By</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Create Item</h4>
        </div>
        <div class="modal-body">
            <form action="{{Route('createitem')}}" method="post">
                @csrf
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Name</label>
                    <input required type="text" value="{{ old('item_name') }}" class="form-control" name="item_name" id="uom_name" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label>Sale Type</label>
                    <select name="sale_type" required class="form-control select2" style="width: 100%;">
                      <option></option>
                      <option {{ old('sale_type') == "1" ? "selected" : ""}} value="1">Cash</option>
                      <option {{ old('sale_type') == "0" ? "selected" : ""}} value="0">Credit</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>UOM</label>
                    <select name="uom" required class="form-control select2" style="width: 100%;">
                      <option></option>
                      @foreach ($uoms as $uom)
                        <option {{ old('uom') == $uom->id ? "selected" : ""}} value="{{$uom->id}}">{{$uom->uom_name}}-{{$uom->uom_quantity}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.content -->
@endsection


