@extends('layouts.app')
@section('title', 'Reports')
@section('content')
    <section class="content-header">
        <h1>Reports</h1>
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Reports</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Profit/Loss Report</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fa fa-plus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad collapse">
                        <form action="{{ Route('profitlossreport') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="from_date">from date <font color="red">*</font></label>
                                        <input type="date" required name="from_date" value="{{ old('from_date') }}"
                                            class="form-control" placeholder="Enter from date">
                                    </div>
                                    <div class="form-group">
                                        <label for="to_date">to date <font color="red">*</font></label>
                                        <input type="date" required name="to_date" value="{{ old('to_date') }}"
                                            class="form-control" placeholder="Enter to date">
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label>Select Item(s)</label>
                                            <select name="items[]" multiple class="form-control">
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->id }}">{{ $item->item_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="expenses" value="selected" id="expenses">
                                        <label class="form-check-label" for="exampleCheck1">Include Expense(s)</label>
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" name="submit" style="align:right;"
                                                class="btn btn-primary">Export to Excel</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-->
            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Current Stock Report</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fa fa-plus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad collapse">
                        <form action="{{ Route('stockreport') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <div class="form-group">
                                            <label>Select Item(s)</label>
                                            <select name="items[]" multiple class="form-control">
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->id }}">{{ $item->item_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" name="submit" style="align:right;"
                                                class="btn btn-primary">Export to Excel</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
@endsection
