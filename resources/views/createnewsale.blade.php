@section('heda')
    @php
    header('Expires: Thu, 19 Nov 1981 08:52:00 GMT'); //Date in the past
    header('Cache-Control: no-store, no-cache, must-revalidate'); //HTTP/1.1
    @endphp
@endsection
@extends('layouts.app')
@section('title', 'New sales')

@section('content')
    <section class="content-header">
        <h1>
            Create New Purchase
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">New Purchase</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- Application buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Click an Item to sale.</h3>
                    </div>
                    <div class="box-body">
                        @foreach ($items as $item)
                            @if ($item->item_current_stock->sum('quantity') >= 50)
                                <a onclick="addItem({{ $loop->iteration }});" id="item{{ $loop->iteration }}"
                                    data-itemidi="i{{ $item->id }}" data-uom_quantityj="j{{ $item->id }}"
                                    data-itemid="{{ $item->id }}"
                                    data-item="{{ $item->item_name }} ({{ $item->item_uom->uom_short_name }})"
                                    data-uom_quantity="{{ $item->item_uom->uom_quantity }}"
                                    data-quantity="{{ $item->item_current_stock->sum('quantity') }}"
                                    data-actual_quantity="{{ $item->item_current_stock->sum('actual_quantity') }}"
                                    data-mrp="{{ $item->item_current_stock->max('selling_price') }}"
                                    data-actual_mrp="{{ $item->item_current_stock->max('actual_selling_price') }}"
                                    data-actual_buying_price="{{ $item->item_current_stock->max('buying_price') }}"
                                    class="btn btn-app">
                                    <span class="badge bg-green">{{ $item->item_current_stock->sum('quantity') }}</span>
                                    <b>{{ $item->item_name }}</b> <br />{{ $item->item_uom->uom_quantity }} in 1
                                    {{ $item->item_uom->uom_short_name }}
                                </a>
                            @elseif ($item->item_current_stock->sum('quantity') < 50 && $item->item_current_stock->sum('quantity') > 20)
                                <a onclick="addItem({{ $loop->iteration }});" id="item{{ $loop->iteration }}"
                                    data-itemidi="i{{ $item->id }}" data-uom_quantityj="j{{ $item->id }}"
                                    data-itemid="{{ $item->id }}"
                                    data-item="{{ $item->item_name }} ({{ $item->item_uom->uom_short_name }})"
                                    data-uom_quantity="{{ $item->item_uom->uom_quantity }}"
                                    data-quantity="{{ $item->item_current_stock->sum('quantity') }}"
                                    data-actual_quantity="{{ $item->item_current_stock->sum('actual_quantity') }}"
                                    data-mrp="{{ $item->item_current_stock->max('selling_price') }}"
                                    data-actual_mrp="{{ $item->item_current_stock->max('actual_selling_price') }}"
                                    data-actual_buying_price="{{ $item->item_current_stock->max('buying_price') }}"
                                    class="btn btn-app">
                                    <span class="badge bg-yellow">{{ $item->item_current_stock->sum('quantity') }}</span>
                                    <b>{{ $item->item_name }}</b> <br />{{ $item->item_uom->uom_quantity }} in 1
                                    {{ $item->item_uom->uom_short_name }}
                                </a>
                            @else
                                <a onclick="addItem({{ $loop->iteration }});" id="item{{ $loop->iteration }}"
                                    data-itemidi="i{{ $item->id }}" data-uom_quantityj="j{{ $item->id }}"
                                    data-itemid="{{ $item->id }}"
                                    data-item="{{ $item->item_name }} ({{ $item->item_uom->uom_short_name }})"
                                    data-uom_quantity="{{ $item->item_uom->uom_quantity }}"
                                    data-quantity="{{ $item->item_current_stock->sum('quantity') }}"
                                    data-actual_quantity="{{ $item->item_current_stock->sum('actual_quantity') }}"
                                    data-mrp="{{ $item->item_current_stock->max('selling_price') }}"
                                    data-actual_mrp="{{ $item->item_current_stock->max('actual_selling_price') }}"
                                    data-actual_buying_price="{{ $item->item_current_stock->max('buying_price') }}"
                                    class="btn btn-app">
                                    <span class="badge bg-red">{{ $item->item_current_stock->sum('quantity') }}</span>
                                    <b>{{ $item->item_name }}</b> <br />{{ $item->item_uom->uom_quantity }} in 1
                                    {{ $item->item_uom->uom_short_name }}
                                </a>
                            @endif
                        @endforeach
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <form onsubmit="document.getElementById('submit').disabled=true;" action="{{ Route('savesale') }}" method="post">
                    @csrf
                    <div class="box box-default">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Customer</label>
                                            <input required type="text" name="customer_name" class="form-control input-sm"
                                                style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Platform</label>
                                            <select required name="platform" class="form-control select2"
                                                style="width: 100%;">
                                                <option></option>
                                                <option {{ old('platform') == '1' ? 'selected' : '' }} value="1">Proforma
                                                </option>
                                                <option {{ old('platform') == '0' ? 'selected' : '' }} value="0">Sales
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Sale Type</label>
                                            <select name="sale_type" required class="form-control select2"
                                                style="width: 100%;">
                                                <option></option>
                                                <option {{ old('sale_type') == '1' ? 'selected' : '' }} value="1">Cash
                                                </option>
                                                <option {{ old('sale_type') == '0' ? 'selected' : '' }} value="0">Credit
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="myTable" class="table table-bordered">
                                    <tr>
                                        <th>Item</th>
                                        <th>UQ</th>
                                        <th>AQ</th>
                                        <th>MRP</th>
                                        <th>Total Price</th>
                                        <th style="width: 35px">Action</th>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td><b>
                                                <div id="grandTotal"></div>
                                            </b></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <center>
                                    <input id="submit" value="Create Sale" type="submit" class="btn btn-success" />
                                </center>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('page_script')
    <script type="text/javascript">
        var n = 1;

        function addItem(y) {
            var data_itemidi = $(document.getElementById('item' + y + '')).attr('data-itemidi');
            var tag = document.querySelector('#' + data_itemidi + '');
            var data_itemid = $(document.getElementById('item' + y + '')).attr('data-itemid');
            var data_item = $(document.getElementById('item' + y + '')).attr('data-item');
            var data_quantity = $(document.getElementById('item' + y + '')).attr('data-quantity');
            var data_uom_quantity = $(document.getElementById('item' + y + '')).attr('data-uom_quantity');
            var data_uom_quantityj = $(document.getElementById('item' + y + '')).attr('data-uom_quantityj');
            var data_mrp = $(document.getElementById('item' + y + '')).attr('data-mrp');
            var data_actual_buying_price = $(document.getElementById('item' + y + '')).attr('data-actual_buying_price');
            var data_actual_quantity = $(document.getElementById('item' + y + '')).attr('data-actual_quantity');
            var data_actual_mrp = $(document.getElementById('item' + y + '')).attr('data-actual_mrp');
            var data_actual_buying_price = $(document.getElementById('item' + y + '')).attr('data-actual_buying_price');
            if (tag == null) {
                var table = document.getElementById("myTable");
                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);
                cell1.innerHTML = data_item + '<input id="uom_quantity' + y + '" type="hidden" name="uom_quantity' + y +
                    '" value="' + data_uom_quantity + '"><input id="' + data_itemidi + '" type="hidden" name="itemid' + y +
                    '" value="' + data_itemid + '">';
                cell2.innerHTML = '<div style="display: inline;"  class="form-group"><input id="quantity' + y +
                    '" step="any" max="' + data_quantity + '" oninput="calculateTotal(' + y + ')" name="quantity' + y +
                    '" value="{{ old("quantity'+y+'") }}" type="number" class="form-control input-sm"></div>';
                cell3.innerHTML = '<div style="display: inline;"  class="form-group"><input id="actual_quantity' + y +
                    '" max="' + data_actual_quantity + '" oninput="calculateTotalActual(' + y + ')" name="actual_quantity' +
                    y + '" value="{{ old("actual_quantity'+y+'") }}" type="number" class="form-control input-sm"></div>';
                cell4.innerHTML = '<div style="display: inline;" class="form-group"><input min="' +
                    data_actual_buying_price + '" id="mrp' + y + '" oninput="calculateTotal(' + y + ')" name="mrp' + y +
                    '" value="{{ old("mrp'+y+'") }}" type="number" class="form-control input-sm"></div>';
                cell5.innerHTML = '<div style="display: inline;" class="form-group"><input id="totalprice' + y +
                    '" disabled="disabled" name="totalprice' + y +
                    '" value="{{ old("totalprice'+y+'") }}" type="number" class="form-control input-sm"></div>';
                cell6.innerHTML = '<div style="display: inline;" class="form-group"><button id="button' + y +
                    '" type="button"  onclick="deleteField(this)" class="btn btn-info btn-sm"><i class="fa fa-minus"></i></button></div>';
                document.getElementById('quantity' + y + '').value = 1;
                var qnty = document.getElementById('quantity' + y + '').value;
                document.getElementById('actual_quantity' + y + '').value = qnty * data_uom_quantity;
                document.getElementById('mrp' + y + '').value = Number(data_mrp);
            } else {
                var qnty = document.getElementById('quantity' + y + '').value;
                document.getElementById('quantity' + y + '').value = Number(1) + Number(qnty);
                qnty = document.getElementById('quantity' + y + '').value;
                document.getElementById('actual_quantity' + y + '').value = qnty * data_uom_quantity;
                document.getElementById('mrp' + y + '').value = Number(data_mrp);
            }

            document.getElementById('totalprice' + y + '').value = Number(qnty) * Number(data_mrp);

            var tags = document.querySelectorAll('*[id^="totalprice"]');
            var sum = 0;
            for (var i = 0; i < tags.length; i++) {
                sum = Number(sum) + Number(tags.item(i).value);
            }
            document.getElementById('grandTotal').innerHTML = '<b>' + sum + '</b>';
            n++;
        }

        function calculateTotal(p) {
            var quantity = document.getElementById('quantity' + p + '').value;
            var mrp = document.getElementById('mrp' + p + '').value;
            var totalprice = quantity * mrp;

            var uom_quantity = document.getElementById('uom_quantity' + p + '').value;
            document.getElementById('actual_quantity' + p + '').value = Number(quantity) * Number(uom_quantity);
            document.getElementById('totalprice' + p + '').value = totalprice;

            var tags = document.querySelectorAll('*[id^="totalprice"]');
            var sum = 0;
            for (var i = 0; i < tags.length; i++) {
                sum = Number(sum) + Number(tags.item(i).value);
            }
            document.getElementById('grandTotal').innerHTML = '<b>' + sum + '</b>';
        }

        function calculateTotalActual(p) {
            var actual_quantity = document.getElementById('actual_quantity' + p + '').value;
            var uom_quantity = document.getElementById('uom_quantity' + p + '').value;
            document.getElementById('quantity' + p + '').value = Number(actual_quantity) / Number(uom_quantity);
            var quantity = document.getElementById('quantity' + p + '').value;
            var mrp = document.getElementById('mrp' + p + '').value;
            var totalprice = quantity * mrp;

            document.getElementById('totalprice' + p + '').value = totalprice;

            var tags = document.querySelectorAll('*[id^="totalprice"]');
            var sum = 0;
            for (var i = 0; i < tags.length; i++) {
                sum = Number(sum) + Number(tags.item(i).value);
            }
            document.getElementById('grandTotal').innerHTML = '<b>' + sum + '</b>';
        }

        function deleteField(x) {
            var i = x.parentNode.parentNode.parentNode.rowIndex;
            document.getElementById('myTable').deleteRow(i);
            var tags = document.querySelectorAll('*[id^="totalprice"]');
            var sum = 0;
            for (var i = 0; i < tags.length; i++) {
                sum = Number(sum) + Number(tags.item(i).value);
            }
            document.getElementById('grandTotal').innerHTML = '<b>' + sum + '</b>';
        }
    </script>
@endsection
