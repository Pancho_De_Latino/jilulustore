@extends('layouts.app')
@section('title', 'Stock')

@section('content')
<section class="content-header">
    <h1>
        Stock
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Current Stock</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Out of Stock</span>
                <span class="info-box-number">{{$outofstock}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Less than 10</span>
                <span class="info-box-number">{{$lessthan10}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Greater than 10</span>
            <span class="info-box-number">{{$greaterthan10}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Item name</th>
                      <th>Status</th>
                      <th>Quantity</th>
                      <th>Group</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        {{-- {{Route('viewgrn',["id" => $item->id])}} --}}
                        <td><a href="{{Route('itemtracking',["id" => $item->id])}}">{{ $item->item_name}}</a></td>
                        <td>
                            @if ($item->is_active==1)
                                Active
                            @else
                                Disabled
                            @endif
                        </td>
                        <td>{{ $item->item_current_stock->sum('quantity')}}</td>
                        <td>
                            @if ($item->item_current_stock->sum('quantity') == 0)
                                out of stock
                            @elseif ($item->item_current_stock->sum('quantity') >0 && $item->item_current_stock->sum('quantity')<=10)
                                less than 10
                            @else
                                greater than 10
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Item name</th>
                        <th>Status</th>
                        <th>Quantity</th>
                        <th>Group</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
    </div>
</section>
  <!-- /.content -->
@endsection


