@extends('layouts.app')
@section('title', 'Edit User')
@section('content')
    @foreach ($user as $userdetails)
        <section class="content-header">
            <h1>Edit User</h1>
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Edit user</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('partial.alert')
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <center>
                        <!-- Profile Image -->
                        <div class="box box-primary box-outline">
                            <div class="box-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img mb-9"
                                        src="{{ is_null($userdetails->profilepath)? asset(Storage::url('public/defaultprofile.png')): asset(Storage::url($userdetails->profilepath)) }}"
                                        alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">{{ $userdetails->user_name }}</h3>


                                <h3 class="profile-username text-center">{{ $userdetails->first_names }}
                                    {{ $userdetails->last_name }}</h3>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </center>
                </div>
                <div class="col-md-3"></div>
            </div>
            <form action="{{ route('edituserprofile') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">User ID</label>
                                    <input type="text" value="{{ $userdetails->user_id }}" disabled class="form-control"
                                        id="exampleInputEmail1" placeholder="Enter User ID">
                                </div>

                                <input type="hidden" name="user_id" value="{{ $userdetails->user_id }}">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">First names</label>
                                    <input type="text" required name="first_names" value="{{ $userdetails->first_names }}"
                                        class="form-control" id="exampleInputEmail1" placeholder="Enter First name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last name</label>
                                    <input type="text" required name="last_name" value="{{ $userdetails->last_name }}"
                                        class="form-control" id="exampleInputEmail1" placeholder="Enter Last name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Phone Number</label>
                                    <input type="text" required name="phone_number" required
                                        value="{{ $userdetails->phone_number }}" class="form-control"
                                        id="exampleInputEmail1" placeholder="Enter Phone number">
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" required name="email" value="{{ $userdetails->email }}"
                                        class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select required name="role" class="form-control select2" style="width: 100%;">
                                        <option>--select role--</option>
                                        @foreach ($roles as $role)
                                            <option {{ $role->id == $userdetails->role_id ? 'selected' : '' }}
                                                value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Profile Picture </label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/png, image/gif, image/jpeg" name="image"
                                                class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                        <center>
                            <button type="submit" class="btn btn-success" style="margin-right: 5px;">
                                Update User Information
                            </button>
                        </center>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </form>
            <br />
            <form action="{{ route('edituserpermissions') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <!-- general form elements disabled -->
                        <div class="box box-secondary">
                            <div class="box-header">
                                <h3 class="box-title">{{ $userdetails->name }} | Permissions</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="box-body">
                                <div class="row">
                                    <input type="hidden" name="user_id" value="{{ $userdetails->user_id }}">
                                    @foreach ($permissions as $permission)
                                        <div class="col-md-3">
                                            <div class="form-check">
                                                <input class="form-check-input" name="{{ $permission->id }}"
                                                    type="checkbox"
                                                    @foreach ($userrolepermission as $userpermission)
                                                    @if ($userpermission->id == $permission->id)
                                                        checked
                                                    @endif
                                                    @endforeach
                                                    @foreach ($userpermissionsviaroles as $userpermissionsviarole)
                                                    @if ($userpermissionsviarole->id == $permission->id)
                                                        disabled
                                                    @endif
                                                    @endforeach>
                                                <label class="form-check-label">{{ $permission->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <center>
                                            <font face="cambria" color="green">Hint**: Disabled Permissions are Role Based
                                                Permissions. Hence, Can not be Edited from This Dashboard</font>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                        <center>
                            <button type="submit" class="btn btn-success float-center" style="margin-right: 5px;">
                                Update User Permissions
                            </button>
                        </center>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </form>
        </section>
    @endforeach
@endsection
