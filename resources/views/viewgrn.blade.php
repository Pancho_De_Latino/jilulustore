@extends('layouts.app')
@section('title', 'View GRN')

@section('content')
<section class="content-header">
    <h1>
        Edit Purchase
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ Route('viewgrns') }}"><i class="fa fa-dashboard"></i> View GRN</a></li>
        <li class="active">Edit Purchase</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            @include('partial.alert')
        </div>
    </div>
    <form action="{{ Route('savepurchase') }}" method="post">
      @csrf
      <div class="box box-default">
          <div class="box">
              <div class="box-header with-border">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label>Supplier</label>
                              <select required name="supplier" class="form-control select2" style="width: 100%;">
                                <option></option>
                                @foreach ($suppliers as $supplier)
                                    <option {{ old("supplier") == $supplier->id ? "selected" : ""}} value="{{ $supplier->id }}">{{ $supplier->supplier_name}}</option>
                                @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label>Purchase Type</label>
                              <select name="purchase_type" required class="form-control select2" style="width: 100%;">
                                <option></option>
                                <option {{ old('purchase_type') == "po" ? "selected" : ""}} value="po">Purchase Order</option>
                                <option {{ old('purchase_type') == "1" ? "selected" : ""}} value="1">Cash GRN</option>
                                <option {{ old('purchase_type') == "0" ? "selected" : ""}} value="0">Credit GRN</option>
                              </select>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
              <table id="myTable" class="table table-bordered">
                    <tr>
                        <th style="width: 180px">Item</th>
                        <th>Quantity</th>
                        <th>Batch No.</th>
                        <th>ExpireDate</th>
                        <th>Buying Price</th>
                        <th>Total Price</th>
                        <th>MRP</th>
                        <th style="width: 90px">Action</th>
                    </tr>
                    <tr id="row0">
                        <td>
                            <div  style="display: inline;" class="form-group">
                                <select onChange="autoFill(0)" name="select0" id="select0" style="width: 100%;" class="form-control select2">
                                <option></option>
                                @foreach ($items as $item)
                                    <option {{ old("select0") == $item->id ? "selected" : ""}}  data-quantity="{{count($item->item_grns) > 0 ? $item->item_grns[0]->quantity:'' }}" data-batchno="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->batch_number:'' }}" data-sellingprice="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->selling_price:'' }}" data-buyingprice="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->buying_price:'' }}" data-expiredate="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->expire_date:'' }}" value="{{$item->id}}">{{$item->item_name}} - {{$item->item_uom->uom_short_name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </td>
                        <td>
                            <div style="display: inline;"  class="form-group">
                                <input id="quantity0" oninput="calculateTotal(0)" name="quantity0" value="{{ old("quantity0") }}" type="number" class="form-control input-sm">
                            </div>
                        </td>
                        <td>
                            <div  style="display: inline;"  class="form-group">
                                <input id="batchno0" name="batchno0" value="{{ old("batchno0") }}" type="text" class="form-control input-sm">
                            </div>
                        </td>
                        <td>
                            <div style="display: inline;" class="form-group">
                                <input id="expiredate0" min="{{$today}}" name="expiredate0" value="{{ old("expiredate0") }}" type="date" class="form-control input-sm">
                            </div>
                        </td>
                        <td>
                            <div style="display: inline;" class="form-group">
                                <input id="buyingprice0" oninput="calculateTotal(0)" name="buyingprice0" value="{{ old("buyingprice0") }}" type="number" class="form-control input-sm">
                            </div>
                        </td>
                        <td>
                        <div style="display: inline;" class="form-group">
                            <input id="totalprice0" disabled="disabled" name="totalprice0" value="{{ old("totalprice0") }}" type="number" class="form-control input-sm">
                        </div>
                        </td>
                        <td>
                            <div style="display: inline;" class="form-group">
                                <input id="mrp0" name="mrp0" value="{{ old("mrp0") }}" type="number" class="form-control input-sm">
                            </div>
                        </td>
                        <td>
                            <div style="display: inline;" class="form-group">
                                <button id="button0" type="button"  onclick="createField()" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td><b><div id="grandTotal"></div></b></td>
                        <td colspan="2"></td>
                    </tr>
              </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                  <center>
                      <button type="submit" class="btn btn-success">Create Purchase</button>
                  </center>
              </div>
          </div>
          <!-- /.box -->
      </div>
    </form>
  </section>
  <!-- /.content -->
@endsection
@section('page_script')
<script type="text/javascript">
	var n=1;
    function createField(){
        var table=document.getElementById("myTable");
        var row=table.insertRow(2);
        var cell1=row.insertCell(0);
        var cell2=row.insertCell(1);
        var cell3=row.insertCell(2);
        var cell4=row.insertCell(3);
        var cell5=row.insertCell(4);
        var cell6=row.insertCell(5);
        var cell7=row.insertCell(6);
        var cell8=row.insertCell(7);
        cell1.innerHTML='<div style="display: inline;" class="form-group"><select onChange="autoFill('+n+')" id="select'+n+'" name="select'+n+'" style="width: 100%;" class="form-control select2"><option></option>@foreach ($items as $item)<option  {{ old("select'+n+'") == $item->id ? "selected" : ""}}  data-quantity="{{count($item->item_grns) > 0 ? $item->item_grns[0]->quantity:'' }}" data-batchno="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->batch_number:'' }}" data-sellingprice="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->selling_price:'' }}" data-buyingprice="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->buying_price:'' }}" data-expiredate="{{ count($item->item_grns) > 0 ? $item->item_grns[0]->expire_date:'' }}" value="{{$item->id}}">{{$item->item_name}} - {{$item->item_uom->uom_short_name}}</option>@endforeach</select></div>';
        cell2.innerHTML='<div style="display: inline;"  class="form-group"><input id="quantity'+n+'"  oninput="calculateTotal('+n+')" name="quantity'+n+'" value="{{ old("quantity'+n+'") }}" type="number" class="form-control input-sm"></div>';
        cell3.innerHTML='<div style="display: inline;"  class="form-group"><input id="batchno'+n+'" name="batchno'+n+'" value="{{ old("batchno'+n+'") }}" type="text" class="form-control input-sm"></div>';
        cell4.innerHTML='<div style="display: inline;" class="form-group"><input id="expiredate'+n+'"  min="{{$today}}" name="expiredate'+n+'" value="{{ old("expiredate'+n+'") }}" type="date" class="form-control input-sm"></div>';
        cell5.innerHTML='<div style="display: inline;" class="form-group"><input id="buyingprice'+n+'"  oninput="calculateTotal('+n+')" name="buyingprice'+n+'" value="{{ old("buyingprice'+n+'") }}" type="number" class="form-control input-sm"></div>';
        cell6.innerHTML='<div style="display: inline;" class="form-group"><input id="totalprice'+n+'" disabled name="sellingprice" value="{{ old("sellingprice'+n+'") }}" type="number" class="form-control input-sm"></div>';
        cell7.innerHTML='<div style="display: inline;" class="form-group"><input id="mrp'+n+'" name="mrp'+n+'" value="{{ old("mrp'+n+'") }}" type="number" class="form-control input-sm"></div>';
        cell8.innerHTML='<div style="display: inline;" class="form-group"><button id="button'+n+'" type="button"  onclick="createField()" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button> <button type="button" id="deletebutton'+n+'" onclick="deleteField(this)" class="btn btn-info btn-sm"><i class="fa fa-minus"></i></button></div>';
        n++;
    }
    function deleteField(x){
        var i= x.parentNode.parentNode.parentNode.rowIndex;
		document.getElementById('myTable').deleteRow(i);
        var tags=document.querySelectorAll('*[id^="totalprice"]');
        var sum=0;
        for(var i=0; i<tags.length;i++){
            sum = Number(sum) + Number(tags.item(i).value);
        }
        document.getElementById('grandTotal').innerHTML='<b>'+sum+'</b>';
	}
    function autoFill(y){
        var quantity=$(document.getElementById('select'+y+'')).find('option:selected').data('quantity');
        var batchno=$(document.getElementById('select'+y+'')).find('option:selected').data('batchno');
        var expiredate=$(document.getElementById('select'+y+'')).find('option:selected').data('expiredate');
        var buyingprice=$(document.getElementById('select'+y+'')).find('option:selected').data('buyingprice');
        var sellingprice=$(document.getElementById('select'+y+'')).find('option:selected').data('sellingprice');
        document.getElementById('quantity'+y+'').value=quantity;
        if(batchno!==undefined)
            document.getElementById('batchno'+y+'').value=batchno;
        else
            document.getElementById('batchno'+y+'').value='';
        document.getElementById('expiredate'+y+'').value=expiredate;
        document.getElementById('buyingprice'+y+'').value=buyingprice;
        document.getElementById('mrp'+y+'').value=sellingprice;

        document.getElementById('mrp'+y+'').setAttribute('min',buyingprice);

        document.getElementById('expiredate'+y+'').value=expiredate;

        var totalprice=quantity*buyingprice;
        document.getElementById('totalprice'+y+'').value=totalprice;

        var tags=document.querySelectorAll('*[id^="totalprice"]');
        var sum=0;
        for(var i=0; i<tags.length;i++){
            sum = Number(sum) + Number(tags.item(i).value);
        }
        document.getElementById('grandTotal').innerHTML='<b>'+sum+'</b>';
	}
    function calculateTotal(p){
        var quantity=document.getElementById('quantity'+p+'').value;
        var buyingprice=document.getElementById('buyingprice'+p+'').value;
        var totalprice=quantity*buyingprice;
        document.getElementById('totalprice'+p+'').value=totalprice;

        var tags=document.querySelectorAll('*[id^="totalprice"]');
        var sum=0;
        for(var i=0; i<tags.length;i++){
            sum = Number(sum) + Number(tags.item(i).value);
        }
        document.getElementById('grandTotal').innerHTML='<b>'+sum+'</b>';
    }
</script>
@endsection
