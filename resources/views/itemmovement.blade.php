@extends('layouts.app')
@section('title', 'Item movement')

@section('content')
    <section class="content-header">
        <h1>
            Item movement
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ Route('viewstock') }}"><i class="fa fa-dashboard"></i> Stock</a></li>
            <li class="active">Item movement</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $itemname }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Transaction</th>
                            <th>Serial No.</th>
                            <th>Date</th>
                            <th>Client/Supplier</th>
                            <th>Quantity</th>
                            <th>Remaining</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $remaining = 0;
                        @endphp
                        @foreach ($itemmovement as $item)
                            <tr>
                                @if ($item->a == 'a')
                                    <td>{{ $loop->iteration }}</td>
                                    <td>Sale</td>
                                    <td>{{ $item->receipt_number }}</td>
                                    <td>{{ $item->transactiondate }}</td>
                                    <td>{{ $item->customer }}</td>
                                    <td>-{{ $item->quantity }}</td>
                                    @php
                                        $remaining -= $item->quantity;
                                    @endphp
                                    <td>{{ $remaining }}</td>
                                @else
                                    <td>{{ $loop->iteration }}</td>
                                    <td>Purchase</td>
                                    <td>{{ $item->receipt_number }}</td>
                                    <td>{{ $item->transactiondate }}</td>
                                    <td>{{ $item->customer }}</td>
                                    <td>+{{ $item->quantity }}</td>
                                    @php
                                        $remaining += $item->quantity;
                                    @endphp
                                    <td>{{ $remaining }}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Transaction</th>
                            <th>Serial No.</th>
                            <th>Date</th>
                            <th>Client/Supplier</th>
                            <th>Quantity</th>
                            <th>Remaining</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
