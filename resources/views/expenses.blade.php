@extends('layouts.app')
@section('title', 'Expenses')

@section('content')
    <section class="content-header">
        <h1>
            Expenses
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Expenses</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <button id="button0" type="button" onclick="createField()" class="btn btn-info btn-sm pull-right"
                        data-toggle="modal" data-target="#modal-default">
                        <i class="fa fa-plus"></i> Add New Expenses
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Expense Category</th>
                                    <th>Expense Details</th>
                                    <th>Amount</th>
                                    <th>Created By</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($expenses as $expense)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $expense->expense_category_details->category_name }}</td>
                                        <td>{{ $expense->details }}</td>
                                        <td>{{ $expense->amount }}</td>
                                        <td>{{ $expense->expense_created_by->first_names }}
                                            {{ $expense->expense_created_by->last_name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Expense Category</th>
                                    <th>Expense Details</th>
                                    <th>Amount</th>
                                    <th>Created By</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create Expense</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ Route('createexpense') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label>Expense Category</label>
                                <select name="expense_category" required class="form-control select2" style="width: 100%;">
                                    <option></option>
                                    @foreach ($expense_categories as $expense_category)
                                        <option {{ old('expense_category') == $expense_category->id ? 'selected' : '' }}
                                            value="{{ $expense_category->id }}">{{ $expense_category->category_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Details</label>
                                <input required type="text" value="{{ old('details') }}" class="form-control"
                                    name="details" id="details" placeholder="Enter Details">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Amount</label>
                                <input required type="number" value="{{ old('amount') }}" class="form-control"
                                    name="amount" id="amount" placeholder="Enter amount">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /.content -->
@endsection
