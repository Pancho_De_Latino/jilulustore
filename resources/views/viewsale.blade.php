@extends('layouts.app')
@section('title', 'View Sales')

@section('content')
<section class="content-header">
    <h1>
        View Sales
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ Route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View sales</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cash Sales</span>
                <span class="info-box-number">{{$cashSale}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Credit Sales</span>
                <span class="info-box-number">{{$creditSale}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
          <!-- /.col -->
        <div class="col-md-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Total Sales</span>
            <span class="info-box-number">{{$totalSale}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Receipt #</th>
                      <th>Item</th>
                      <th>GRN</th>
                      <th>Customer</th>
                      <th>Sale Type</th>
                      <th>Quantity</th>
                      <th>Amount</th>
                      <th>TotalAmount</th>
                      <th>Created By</th>
                      <th>GRN Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        use App\Models\Item;
                        use App\Models\UnitOfMeasure;
                        use App\Models\Grn;
                        use App\User;
                    @endphp
                    @foreach ($saleitems as $saleitem)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><a href="{{Route('viewonesale',["id" => $saleitem->sale_id])}}">{{ $saleitem->sale_details->receipt_number }}</a></td>
                        <td>{{Item::find($saleitem->item_stock_details->item_id)->item_name}} - {{ UnitOfMeasure::find(Item::find($saleitem->item_stock_details->item_id)->unit_of_measure)->uom_short_name }}</td>
                        <td><a href="{{Route('viewgrn',["id" => $saleitem->item_stock_details->grn_id])}}">{{ Grn::find($saleitem->item_stock_details->grn_id)->grn_number}}</a></td>
                        <td>{{ $saleitem->sale_details->customer }}</td>
                        <td>
                            @if ($saleitem->sale_details->is_cash==1)
                                Cash
                            @else
                                Credit
                            @endif
                        </td>
                        <td>{{ $saleitem->quantity}}</td>
                        <td>{{ $saleitem->amount}}</td>
                        <td>{{ $saleitem->amount * $saleitem->quantity}}</td>
                        <td>{{ User::find($saleitem->sale_details->created_by)->first_names}} {{ User::find($saleitem->sale_details->created_by)->first_names }}</td>
                        <td>{{ $saleitem->sale_details->created_at}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Receipt #</th>
                        <th>Item</th>
                        <th>GRN</th>
                        <th>Customer</th>
                        <th>Sale Type</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th><span id="totalAmount"></span></th>
                        <th>Created By</th>
                        <th>GRN Date</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
    </div>
</section>
  <!-- /.content -->
@endsection
@section('page_scripti')
    <script>
        jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
            return this.flatten().reduce( function ( a, b ) {
                if ( typeof a === 'string' ) {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if ( typeof b === 'string' ) {
                    b = b.replace(/[^\d.-]/g, '') * 1;
                }

                return a + b;
            }, 0 );
        } );
        $('#example1').DataTable({
            drawCallback: function (){
                var api=this.api();
                sum = api.column(8,{page: 'current'}).data().sum();
                document.getElementById('totalAmount').innerHTML='<b>'+sum+'</b>';
            }
        });
    </script>
@endsection
